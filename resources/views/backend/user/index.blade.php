@extends('backend.layouts.master')

@section('title', 'Users')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4>
                        All Users
                        <a class="btn btn-info ml-4">
                            @php $u_total = "0"; @endphp
                            @foreach ($users as $user)
                                @php
                                    if ($user->isUserOnline())
                                    {
                                        $u_total = $u_total + 1;
                                    }
                                @endphp
                            @endforeach
                            Online Users: {{ $u_total }}
                        </a>
                        <a class="btn btn-primary float-right" href="{{route('userCreate')}}">Add New</a>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @include('backend.layouts.flash-message')
                </div>
                <div class="card-body">
                    @if(count($users) != 0)
                        <table id="userTable" class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Availability</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ?>
                                @foreach ($users as $user)
                                    <tr>
                                        <td><b>{{ $i++ }}</b></td>
                                        <td>{{ $user->fullName }}</td>
                                        <td>
                                            <img src="{{ asset($user->image ? $user->image : '') }}" alt="User Profile" style="width:60px;height:60px"/>
                                        </td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->mobile }}</td>
                                        <td>{{ $user->role }}</td>
                                        <td>
                                            @if( $user->status == 0 )
                                                <label class="py-2 px-3 badge btn-success">Unblock</label>
                                            @elseif($user->status == 1)
                                                <label class="py-2 px-3 badge btn-warning">Block</label>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($user->isUserOnline())
                                                <label class="py-2 px-3 badge btn-success">Online</label>
                                            @else
                                                <label class="py-2 px-3 badge btn-warning">Offline</label>
                                            @endif
                                        </td>
                                        <td class="d-flex">
                                            <a href="{{ route('userEdit', $user->id) }}" class="btn btn-sm">
                                                <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect width="33" height="33" rx="8" fill="#445260"/>
                                                    <path d="M15.75 9H14.25C10.5 9 9 10.5 9 14.25V18.75C9 22.5 10.5 24 14.25 24H18.75C22.5 24 24 22.5 24 18.75V17.25" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M19.5299 9.76495L13.6199 15.6749C13.3949 15.8999 13.1699 16.3424 13.1249 16.6649L12.8024 18.9224C12.6824 19.7399 13.2599 20.3099 14.0774 20.1974L16.3349 19.8749C16.6499 19.8299 17.0924 19.6049 17.3249 19.3799L23.2349 13.4699C24.2549 12.4499 24.7349 11.2649 23.2349 9.76495C21.7349 8.26495 20.5499 8.74495 19.5299 9.76495Z" stroke="white" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M18.6826 10.6125C19.1851 12.405 20.5876 13.8075 22.3876 14.3175" stroke="white" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                            </a>
                                            <a href="{{ route('userShow', $user->id) }}" class="btn btn-success mr-2">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('userDelete', $user->id) }}" class="btn btn-sm">
                                                <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect width="33" height="33" rx="8" fill="#F30000"/>
                                                    <path d="M23.3027 11.4225C22.0952 11.3025 20.8877 11.2125 19.6727 11.145V11.1375L19.5077 10.1625C19.3952 9.4725 19.2302 8.4375 17.4752 8.4375H15.5102C13.7627 8.4375 13.5977 9.4275 13.4777 10.155L13.3202 11.115C12.6227 11.16 11.9252 11.205 11.2277 11.2725L9.69774 11.4225C9.38274 11.4525 9.15774 11.73 9.18774 12.0375C9.21774 12.345 9.48774 12.57 9.80274 12.54L11.3327 12.39C15.2627 12 19.2227 12.15 23.1977 12.5475C23.2202 12.5475 23.2352 12.5475 23.2577 12.5475C23.5427 12.5475 23.7902 12.33 23.8202 12.0375C23.8427 11.73 23.6177 11.4525 23.3027 11.4225Z" fill="white"/>
                                                    <path d="M21.9223 13.605C21.7423 13.4175 21.4948 13.3125 21.2398 13.3125H11.7598C11.5048 13.3125 11.2498 13.4175 11.0773 13.605C10.9048 13.7925 10.8073 14.0475 10.8223 14.31L11.2873 22.005C11.3698 23.145 11.4748 24.57 14.0923 24.57H18.9073C21.5248 24.57 21.6298 23.1525 21.7123 22.005L22.1773 14.3175C22.1923 14.0475 22.0948 13.7925 21.9223 13.605ZM17.7448 20.8125H15.2473C14.9398 20.8125 14.6848 20.5575 14.6848 20.25C14.6848 19.9425 14.9398 19.6875 15.2473 19.6875H17.7448C18.0523 19.6875 18.3073 19.9425 18.3073 20.25C18.3073 20.5575 18.0523 20.8125 17.7448 20.8125ZM18.3748 17.8125H14.6248C14.3173 17.8125 14.0623 17.5575 14.0623 17.25C14.0623 16.9425 14.3173 16.6875 14.6248 16.6875H18.3748C18.6823 16.6875 18.9373 16.9425 18.9373 17.25C18.9373 17.5575 18.6823 17.8125 18.3748 17.8125Z" fill="white"/>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center">
                            <div>!!! No Users Found !!!</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
        $('#userTable').DataTable();
        } );
    </script>
@endsection
