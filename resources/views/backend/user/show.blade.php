@extends('backend.layouts.master')

@section('title', 'Show User')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4>Show User</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input name="fullName" type="text" value="{{ $user->fullName }}" placeholder="Enter User Name" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>User Role</label>
                                <input name="role" type="text" value="{{ $user->role }}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Mobile</label>
                                <input name="mobile" type="tel" value="{{ $user->mobile }}" placeholder="Enter User Mobile" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" value="{{ $user->email }}" placeholder="Enter User Email"  class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>State</label>
                                <input name="state" type="text" value="{{ $user->state }}" placeholder="Enter State" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>City</label>
                                <input name="city" type="text" value="{{ $user->city }}" placeholder="City" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Address</label>
                                <input name="address" type="text" value="{{ $user->address }}" placeholder="Address" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Status</label>
                                <input name="status" type="text" value="{{ $user->status ? 'Unblock' : 'Block' }}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <img src="{{ asset($user->image) }}" class="imagePreview mt-2" style="width: 100px; height: 100px"/>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('users') }}" class="btn btn-success float-right mt-2">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.userProfile').on('change', function() {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.imagePreview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        });
    </script>
@endsection
