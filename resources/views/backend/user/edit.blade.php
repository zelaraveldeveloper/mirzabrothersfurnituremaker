@extends('backend.layouts.master')

@section('title', 'Edit User')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4>Edit User</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('userUpdate', $user->id)}}" class="" method ="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input name="fullName" type="text" value="{{ $user->fullName }}" placeholder="Enter User Name" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>User Role</label>
                                    <select class="form-control" name="role">
                                        <option selected>{{$user->role}}</option>
                                        <option value="vendor">Vendor</option>
                                        <option value="user">User</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input name="mobile" type="tel" value="{{ $user->mobile }}" placeholder="Enter User Mobile" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input name="email" type="email" value="{{ $user->email }}" placeholder="Enter User Email"  class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input name="password" type="password" placeholder="Enter User Password" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <input name="state" type="text" value="{{ $user->state }}" placeholder="Enter State" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input name="city" type="text" value="{{ $user->city }}" placeholder="City" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input name="address" type="text" value="{{ $user->address }}" placeholder="Address" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                        <option selected value="0" {{ $user->status == '0' ? 'selected' : '' }}>Unblock</option>
                                        <option value="1" {{ $user->status == '1' ? 'selected' : '' }}>Block</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="userProfile form-control" name="image">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <img src="{{ asset($user->image) }}" class="imagePreview mt-2" style="width: 100px; height: 100px"/>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success float-right mt-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.userProfile').on('change', function() {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.imagePreview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        });
    </script>
@endsection
