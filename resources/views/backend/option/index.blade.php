@extends('backend.layouts.master')

@section('title', 'Options')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4>
                        Options
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @include('backend.layouts.flash-message')
                </div>
                <div class="card-body">
                    <form action="{{ route('optionUpdate', $option->id) }}"  method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Site Title</label>
                                    <input name="title" value="{{ $option->title ?? '' }}" type="text" class="form-control" placeholder="Site Title"/>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Logo</label>
                                    <input type="file" class="form-control siteLogo" name="image">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    @if($option->image)
                                        <img src="{{ asset('upload/web/images/'.$option->image) }}" class="logoPreview mt-2" style="width: 100px; height: 100px"/>
                                    @else
                                        <img src="{{ asset('img/default-user-avatar.png') }}" class="mt-2" style="width: 100px; height: 100px"/>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input name="email" value="{{ $option->email ?? '' }}" type="email" class="form-control" placeholder="Email" />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input name="phone" value="{{ $option->phone ?? '' }}" type="text" class="form-control" placeholder="Phone" />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Footer Text</label>
                                    <input name="footerText" value="{{ $option->footerText ?? '' }}" type="text" class="form-control" placeholder="Footer Text" />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Site Description</label>
                                    <textarea name="description" type="text" placeholder="Site Description" class="form-control" rows="5">{{ $option->description ?? '' }}</textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Contact Address</label>
                                    <textarea name="address" type="text" class="form-control address" rows="5">{{ $option->address ?? '' }}</textarea>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Currency Symbol</label>
                                    <input type="text" class="form-control " name="currency" value="{{ $option->currency ?? '' }}">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Facebook URL</label>
                                    <input name="facebook" value="{{ $option->facebook ? $option->facebook : '' }}" type="text" class="form-control" placeholder="Facebook URL" />
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Instagram URL</label>
                                    <input name="instagram" value="{{ $option->instagram ? $option->instagram : '' }}" type="text" class="form-control" placeholder="Instagram URL" />
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success float-right mt-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/dashboard/option.js') }}"></script>
@endpush
