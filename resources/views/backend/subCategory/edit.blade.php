@extends('backend.layouts.master')

@section('title', 'Edit Sub-Category')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4>Edit Sub Category</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('subCategoryUpdate', [$subCategory->id])}}" class="" method="POST" autocomplete="off">
                        @csrf
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-3"></div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Category Id (Name)</label>
                                        <select name="categoryId" class="form-control">
                                            <option value="{{ $subCategory->categoryId }}">{{$subCategory->category->name}}</option>
                                            @foreach (App\Models\Category::where('userId', Auth::id())->get() as $categoryItem)
                                                <option value="{{ $categoryItem->id }}">{{ $categoryItem->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6"></div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" value="{{ $subCategory->name }}" class="form-control" placeholder="Enter Name"/>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
