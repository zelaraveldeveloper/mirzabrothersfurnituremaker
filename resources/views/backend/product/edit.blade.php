@extends('backend.layouts.master')

@section('title', 'Edit Product')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4>Edit Product</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('productUpdate', $product->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-3"></div>
                                <div class="col-9">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Product Name</label>
                                            <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Product Name" />
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label>Select Category</label>
                                                <select class="form-control" name="categoryId">
                                                    <option value="{{ $product->categoryId }}">{{ $product->category->name }}</option>
                                                        @foreach(App\Models\Category::where('userId',Auth::user()->id)->get() as $category)
                                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label>Select Sub-Category</label>
                                                <select class="form-control" name="subCategoryId">
                                                    <option value="{{ $product->subCategoryId }}">{{ $product->subcategory->name }}</option>
                                                        @foreach(App\Models\SubCategory::where('userId',Auth::user()->id)->get() as $subCategory)
                                                            <option value="{{ $subCategory->id }}">{{ $subCategory->name }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label>Select Brand</label>
                                                <select class="form-control" name="brandId">
                                                    <option value="{{ $product->brandId }}">{{ $product->brand->name ?? 'Select Brand' }}</option>
                                                        @foreach(App\Models\Brand::where('userId',Auth::user()->id)->get() as $brand)
                                                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Small Description</label>
                                            <textarea rows="5" id="smallDescription" class="form-control" name="smallDescription" placeholder="Small Description About Product">{{ $product->smallDescription }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input type="file" class="productImage form-control" name="image">
                                        </div>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <div class="form-group">
                                            <img src="{{ asset( $product->image )}}" class="imagePreview mt-2" style="width: 100px; height: 100px"/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Quantity</label>
                                            <input type="number" name="quantity" value="{{ $product->quantity }}" placeholder="Quantity" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Original Price</label>
                                        <input type="number" name="originalPrice" value="{{ $product->originalPrice }}" placeholder="Original Price" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Offer Price</label>
                                        <input type="number" name="offerPrice" value="{{ $product->offerPrice }}" placeholder="Offer Price" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Sale Price</label>
                                        <input type="number" name="salePrice" value="{{ $product->salePrice }}" placeholder="Sale Price" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control" name="status">
                                            <option value="ACTIVE" {{ $product->status == 'ACTIVE' ? 'selected' : '' }}>Publish</option>
                                            <option value="INACTIVE" {{ $product->status == 'INACTIVE' ? 'selected' : '' }}>Draft</option>
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="col-3">
                                    <div class="form-group mt-3">
                                        <label>Sale Tag</label>
                                        <select class="form-control" name="sale_tag">
                                            <option value="">Select Sale-Tag</option>
                                            <option value="">New</option>
                                            <option value="">Popular</option>
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-9  d-flex mt-4">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label>New</label>
                                            <input type="checkbox" {{ $product->isNew == '1' ? 'checked' : '' }} name="isNew" />
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label>Popular</label>
                                            <input type="checkbox" {{ $product->isPopular == '1' ? 'checked' : '' }} name="isPopular" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                       <button type="submit" class="btn btn-success float-right mt-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.productImage').on('change', function() {
                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                    $('.imagePreview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        });

        // $('#smallDescription').summernote({
        //     placeholder: 'Hello Bootstrap 4',
        //     tabsize: 2,
        //     height: 100
        // });

        // $('#productHighLight').summernote({
        //     Hello Bootstrap 4',
        //     tabsize: 2,
        //     height: 100
        // });

        // $('#productDescription').summernote({
        //     placeholder: 'Hello Bootstrap 4',
        //     tabsize: 2,
        //     height: 100
        // });

        // $('#productSpecification').summernote({
        //     placeholder: 'Hello Bootstrap 4',
        //     tabsize: 2,
        //     height: 100
        // });
    </script>
@endsection
