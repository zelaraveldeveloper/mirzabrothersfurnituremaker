@extends('backend.layouts.master')

@section('title', 'Dashboard')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12 pt-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="far fa-object-group"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Brands</span>
                        <span class="info-box-number">{{ $brands }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12 pt-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="fa fa-sitemap fa-folder"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Categories</span>
                        <span class="info-box-number">{{ $categories }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12 pt-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="fa fa-list"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Sub Categories</span>
                        <span class="info-box-number">{{ $subCategories }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12 pt-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="fa fa-cubes"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Products</span>
                        <span class="info-box-number">{{ $products }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12 pt-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="fa fa-table"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Orders</span>
                        <span class="info-box-number">{{ $orders }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12 pt-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info">
                        <i class="fa fa-user"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Users</span>
                        <span class="info-box-number">{{ $users }}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
