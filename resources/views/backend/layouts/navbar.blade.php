<nav class="main-header navbar navbar-expand navbar-white navbar-light">

    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto mr-4">
        <div class="dropdown">
            <a href="" class="dropdown-toggle logout" data-toggle="dropdown">
                Hi {{ Auth::user()->fullName }}
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('changePassword') }}">Change Password</a></li>
                <li><a href="{{ route('logout') }}">Logout</a></li>
            </ul>
        </div>
    </ul>
</nav>
