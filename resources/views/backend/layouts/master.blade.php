<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ZeeshanNisar') }} | @yield('title')</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

        <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

        <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

        <link rel="stylesheet" href="{{ asset('css/dashboard/dashboard.css') }}">

        <!----- Alertify Css----->
        <link rel="stylesheet" href="{{ asset('css/alertify.min.css') }}"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" />
        @stack('style')
    </head>



    <body class="hold-transition sidebar-mini pace-purple" >
        <div class="wrapper" >

            @include('backend.layouts.navbar')

            @include('backend.layouts.sidebar')

            <div class="content-wrapper" id="app">
                @yield('content')
            </div>

            @include('backend.layouts.footer')

            <aside class="control-sidebar control-sidebar-dark">
            </aside>
        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

        <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

        <script src="{{ asset('dist/js/adminlte.js') }}"></script>

        <!----- Alertify Js----->
        <script src="{{ asset('js/alertify.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        @stack('script')
    </body>
</html>
