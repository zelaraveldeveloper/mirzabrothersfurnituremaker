<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">
        <div class="user-panel mt-2 pt-1 pb-2 mb-2 d-flex">
            <div class="image">
                @if(Auth::user()->image)
                    <img src="{{ asset('upload/user/images/' . Auth::user()->image) }}" class="img-circle elevation-2" alt="User Image">
                @else
                    <img src="img/default-user-avatar.png" class="img-circle elevation-2" alt="Default User Image">
                @endif
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->fullName }}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link {{ Request::path() == 'dashboard' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('categories') }}" class="nav-link {{ Request::path() == 'category' ? 'active' : '' }}">
                        <i class="fa fa-sitemap fa-folder mr-3"></i>
                        Categories
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('subCategories') }}" class="nav-link {{ Request::path() == 'subCategory' ? 'active' : '' }}">
                        <i class="fa fa-list mr-3" aria-hidden="true"></i>
                        Sub-Categories
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('products') }}" class="nav-link {{ Request::path() == 'product' ? 'active' : '' }}">
                        <i class="fa fa-cubes mr-3"></i>
                        Products
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('brands') }}" class="nav-link {{ Request::path() == 'brand' ? 'active' : '' }}">
                        <i class="fa fa-object-group mr-3"></i>
                        Brands
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('users') }}" class="nav-link {{ Request::path() == 'users' ? 'active' : '' }}">
                        <i class="fa fa-users mr-3"></i>
                        Users
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('banners') }}" class="nav-link {{ Request::path() == 'banners' ? 'active' : '' }}">
                        <i class="fa fa-cubes mr-3"></i>
                        Banners
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('options') }}" class="nav-link {{ Request::path() == 'options' ? 'active' : '' }}">
                        <i class="fa fa-image mr-3"></i>
                        Options
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>


        {{--
        <li class="active">
            <a href="{{ route('admin.orders') }}">
                <i class="fa fa-table mr-3"></i>
                Orders
            </a>
        </li>
         --}}
        {{-- <li class="active">
            <a href="{{ route('admin.coupons') }}">
                <i class="fa fa-image mr-3"></i>
                Coupons
            </a>
        </li> --}}
