@extends('frontend.layouts.master')

@section('title', 'Single Product')

@push('style')
    <link href="{{ asset('css/singleProduct.css') }}" rel="stylesheet">
@endpush

@section('content')
    <section class="content">
        <div class="single-product-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <ol class="breadcrumb">
                            <li><a>Collection /</a></li>
                            <li><a>{{ $product->subCategory->name }} /</a></li>
                            <li><a>{{ $product->name }}</a></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <div class="product-image">
                            <img id="product-img" src="{{ asset($product->image) }}" alt=""/>
                        </div>
                    </div>
                     <div class="col-md-1"></div>
                        <div class="col-md-5">
                            <div class="product-content">
                                <h3 class="title">{{ $product->name }}</h3>
                                <span class="price">{{ config('app.currency_symbol') }}: {{$product->salePrice}}</span>
                                <p class="description">{{ $product->smallDescription }}</p>
                                <input type="hidden" class="product_id" value="{{$product->id}}" />
                                <input type="number" class="qty-input form-control" value="1" min="1" max="100"/>
                                <a class="add-to-cart" data-productid="{{$product->id}}">Add to cart</a>
                                @guest
                                    <a class="" href="#" data-toggle="modal" data-target="#userLogin">
                                        Add to Wishlist
                                    </a>
                                @else
                                    <a class="add-to-wishlist" data-productid="{{$product->id}}">
                                        Add to Wishlist
                                    </a>
                                @endguest
                            </div>
                        </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{asset('js/singleProduct.js')}}"></script>
@endpush
