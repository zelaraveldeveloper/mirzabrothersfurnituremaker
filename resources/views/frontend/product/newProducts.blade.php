@extends('frontend.layouts.master')

@section('title', 'New Arrivals')

@push('style')
    <link href="{{ asset('css/newProduct.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>New Arrivals</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="row">
                        @if(count($newProducts)!=0)
                            @foreach ($newProducts as $newItem)
                                <div class="col-md-3 mt-3">
                                    <div class="card">
                                        <div class="product-image">
                                            <img id="product-img" src="{{ asset($newItem->image) }}" alt=""/>
                                        </div>
                                        <div class="card-body text-center">
                                            <h5 class="font-weight-bold text-truncate">{{ $newItem->name }}</h5>
                                            <h6>
                                                <span class="font-italic mr-2"> <s>Rs: {{ $newItem->offerPrice }} </s> </span>
                                                <span class="font-weight-bold"> Rs: {{ $newItem->salePrice }} </span>
                                            </h6>
                                            <a href="{{ url('collection/'. $newItem->subCategory->name.'/'.$newItem->name) }}" class="btn btn-outline-primary mt-2">
                                                View Details
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <div class="card mb-4">
                                    <div class="card-body">
                                        <h2 class="text-center font-weight-bold">!!! No New Product Available !!!</h2>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
