@extends('frontend.layouts.master')

@section('title', 'Products')

@push('style')
    <link href="{{ asset('css/frontend/product.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="section-head mb-1">Collection / {{$subCategory->name}}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-mb-12 mb-3 ml-3 ">
                <div class="card">
                    <div class="card-body">
                        <span class="font-weight-bold sort-font">Sort By :</span>
                        <a href="{{ URL::current() }}" class="sort-font pl-3">All</a><b class="pl-3">|</b>
                        <a href="{{ URL::current()."?sort=price_asc" }}" class="sort-font pl-3">Price - Low to High</a><b class="pl-3">|</b>
                        <a href="{{ URL::current()."?sort=price_desc" }}" class="sort-font pl-3">Price - High to Low</a><b class="pl-3">|</b>
                        <a href="{{ URL::current()."?sort=newest" }}" class="sort-font pl-3">Newest</a><b class="pl-3">|</b>
                        <a href="{{ URL::current()."?sort=popularity" }}" class="sort-font pl-3">Popularity</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <form action="{{ URL::current() }}" method="GET">
                    <div class="card">
                        <div class="card-header">
                            <h5>
                                Related Brand
                                <button type="submit" class="btn btn-primary btn-sm float-right">Filter</button>
                            </h5>
                        </div>
                        <div class="card-body">
                            @foreach ($brandList as $itemBrand)
                                @php
                                    $checked = [];
                                        if(isset($_GET['filterBrand']))
                                        {
                                            $checked = $_GET['filterBrand'];
                                        }
                                @endphp
                                <div class="mb-1">
                                    <input type="checkbox" name="filterBrand[]" value="{{ $itemBrand->name }}"
                                        @if (in_array($itemBrand->name, $checked)) checked  @endif
                                    />
                                        {{ $itemBrand->name }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-9">
                <div class="card mb-4">
                    <div class="card-body">
                        @if(count($products)!=0)
                            @foreach ($products as $productItem)
                                <div class="col-md-12 mb-3">
                                    <div class="card product_data">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="product-image">
                                                        <img id="product-img" src="{{ asset($productItem->image) }}" alt=""/>
                                                    </div>
                                                    <div class="wislist-content">
                                                        @guest
                                                            <a class="btn btn-danger btn-block shadow" href="#" data-toggle="modal" data-target="#userLogin">
                                                                <i class="fa fa-heart mr-2"></i>
                                                                Add to Wishlist
                                                            </a>
                                                        @else
                                                            <a class="add-to-wishlist btn btn-danger btn-block shadow" data-productid="{{$productItem->id}}">
                                                                <i class="fa fa-heart mr-2"></i>
                                                                Add to Wishlist
                                                            </a>
                                                        @endguest
                                                    </div>
                                                </div>
                                                <div class="col-md-7 border-right border-left">
                                                    <a href="{{ url('collection/' . $productItem->subCategory->name . '/' . $productItem->name) }}" class="text-center">
                                                        <h5 class="mb-2"><b>{{ $productItem->name }}</b></h5>
                                                    </a>
                                                    <div class="">
                                                        <h6 class="text-dark mb-0">{!! $productItem->smallDescription !!}</h6>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="text-right">
                                                        {{-- <h6 class="font-italic text-dark badge badge-warning px-3 py-2">{{$productItem->sale_tag}}</h6> --}}
                                                        <h6 class="font-italic mt-2 py-2 font-weight-bold"><s> {{ config('app.currency_symbol') }}: {{ number_format($productItem->offerPrice) }}</s></h6>
                                                        <h5 class="font-italic font-weight-bold mt-2"> {{ config('app.currency_symbol') }}: {{number_format($productItem->salePrice) }}</h4>
                                                    </div>
                                                    <div class="text-right">
                                                        <div class="mt-2 py-2">
                                                            <a style="font-size: 16px" href="{{ url('collection/' . $productItem->subCategory->name . '/' . $productItem->name) }}" class="btn btn-outline-primary py-1 px-2">
                                                                View Details
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <div class="card mb-4">
                                    <div class="card-body">
                                        <h2 class="text-center font-weight-bold">!!! No Product Available !!!</h2>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
