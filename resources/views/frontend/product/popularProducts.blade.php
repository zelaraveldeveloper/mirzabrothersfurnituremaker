@extends('frontend.layouts.master')

@section('title', 'Popular Products')

@push('style')
    <link href="{{ asset('css/popularProduct.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>Popular Products</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="row">
                        @if(count($popularProducts)!=0)
                            @foreach ($popularProducts as $popularItem)
                                <div class="col-md-3 mt-3">
                                    <div class="card">
                                        <div class="product-image">
                                            <img id="product-img" src="{{ asset($popularItem->image) }}" alt=""/>
                                        </div>
                                        <div class="card-body text-center">
                                            <h5 class="font-weight-bold text-truncate">{{ $popularItem->name }}</h5>
                                            <h6>
                                                <span class="font-italic mr-2"> <s>Rs: {{ $popularItem->offerPrice }} </s> </span>
                                                <span class="font-weight-bold"> Rs: {{ $popularItem->salePrice }} </span>
                                            </h6>
                                            <a href="{{ url('collection/'. $popularItem->subCategory->name.'/'.$popularItem->name) }}" class="btn btn-outline-primary mt-2">
                                                View Details
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <div class="card mb-4">
                                    <div class="card-body">
                                        <h2 class="text-center font-weight-bold">!!! No Popular Product Available !!!</h2>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
