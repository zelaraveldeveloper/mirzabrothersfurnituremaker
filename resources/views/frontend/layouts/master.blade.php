<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @php
        $option = app('globalData')['option'];
        $subCategories  = app('globalData')['subCategories'];
        $countries = app('globalData')['countries'];
    @endphp

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'ZeeshanNisar') }} | @yield('title')</title>

        <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">

        <link rel="stylesheet" href="{{ asset('css/jsdelivr.net_npm_bootstrap@4.0.0_dist_css_bootstrap.min.css') }}">

        <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

        <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

        <link rel="stylesheet" href="{{ asset('css/frontend.css') }}">

        <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">

        <link rel="stylesheet" href="{{ asset('css/owl.theme.min.css') }}">

        <link href="{{ asset('css/auth/auth.css') }}" rel="stylesheet">

        <!----- Alertify Css----->
        <link rel="stylesheet" href="{{ asset('css/alertify.min.css') }}"/>

        @stack('style')
    </head>
    <body>
        <div class="wrapper" id="app">
            @include('frontend.layouts.navbar')

            @yield('content')

            @include('frontend.layouts.footer')
        </div>

        <script src="{{ asset('js/jquery.slim.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>

        <!----- Alertify Js----->
        <script src="{{ asset('js/alertify.min.js') }}"></script>

        {{-- <script>
            if (session('status')) {
                alertify.set('notifier','position','top-right');
                alertify.success("{{ session('status') }}");
            }
        </script> --}}
        @stack('script')
    </body>
</html>
