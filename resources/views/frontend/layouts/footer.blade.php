<footer>
    <div id ="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3>{{ $option->title ?? ' ' }}</h3>
                    <p>{{ $option->description ?? ' ' }}</p>
                </div>
                <div class="col-md-3">
                    <h3>Categories</h3>
                    <ul class="menu-list">
                        @foreach ($subCategories as $subCategory)
                            <li><a href="">{{ $subCategory->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Useful Links</h3>
                    <ul class="menu-list">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('allProducts') }}">All Products</a></li>
                        <li><a href="{{ route('newArrival') }}">Latest Products</a></li>
                        <li><a href="{{ route('popularProducts') }}">Popular Products</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Contact Us</h3>
                    <ul class="menu-list">
                        <li><i class="fa fa-home" ></i><span>: {{ $option->address ?? ' ' }}</span></li>
                        <li><i class="fa fa-phone" ></i><span>: {{ $option->phone ?? ' ' }}</span></li>
                        <li><i class="fa fa-envelope" ></i><span>: {{ $option->email ?? ' ' }}</span></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <span>{{ $option->footerText ?? ' ' }} | Created by <a href="{{ route('home') }}" target="_blank">ZeeshanNisar</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
