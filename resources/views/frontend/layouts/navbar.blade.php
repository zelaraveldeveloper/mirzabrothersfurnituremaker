<div id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="{{ route('home') }}" class="logo-img">
                    @if(file_exists(public_path($option->image)))
                        <img src="{{ asset($option->image) }}" alt="Site Logo">
                    @else
                        <img src="{{ asset('img/default-user-avatar.png') }}" alt="Default Image">
                    @endif
                </a>
            </div>
            <div class="col-md-7">
                <form id="search-form" action="{{ url('/searching') }}" method="POST">
                @csrf
                    <div class="input-group search">
                        <input name="searchProduct" id="search_text" type="text" class="form-control" placeholder="Search for...">
                        <button type="submit" name="searchBtn" class="input-group-text btn" id="basic-addon2">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>

            <div class="col-md-3">
                <ul class="header-info">
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="caret"></i>
                            <i class="fa fa-user"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-list">
                            @auth
                                <li><a href="{{ route('userProfile') }}" class="" >My Profile</a></li>
                                {{-- <li><a href="" class="" >My Orders</a></li> --}}
                                <li>
                                    <a href="{{ route('logout_user') }}" class="" >Logout</a></li>
                            @endauth
                            @guest
                                <li><a data-toggle="modal" data-target="#userLogin" href="#">login</a></li>
                                <li><a class="signup-button-click" data-toggle="modal" data-target="#userSignup" href="#">Register</a></li>
                            @endguest
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('userWishlist') }}">
                            <i class="fa fa-heart"  id="wishlist-count"></i>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('userCart') }}">
                            <i class="fa fa-shopping-cart" id="cart-count"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="header-menu">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <ul class="menu-list">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    @foreach ($subCategories as $subCategory)
                        <li><a href="{{ url('collection/'. $subCategory->name) }}">{{ $subCategory->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@include('generalModals.login')
@include('generalModals.signup')
