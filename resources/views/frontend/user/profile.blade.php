@extends('frontend.layouts.master')

@section('title', 'User Profile')

@section('content')
    <div id="user_profile-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card  mb-3">
                        <div class="card-body">
                            <h2>My Profile</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card  mb-3">
                        <div class="card-body">
                            <form action="{{route('profileUpdate')}}" method="post" enctype="multipart/form-data">
                            @csrf
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group w-70 h-90">
                                            <img src="{{ asset(Auth::user()->image) }}" style="width:100px; height:100px" alt="User Profile" />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" value="{{ Auth::user()->fullName }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" class="form-control" value="{{Auth::user()->email}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input type="text" class="form-control" value="{{Auth::user()->mobile}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input type="text" class="form-control" value="{{Auth::user()->country->name}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <input type="text" class="form-control" value="{{Auth::user()->state->name}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control" value="{{Auth::user()->city}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text" class="form-control" value="{{Auth::user()->address}}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <a class="btn btn-secondary ml-3" type="submit">Update Profile</a>
                                <a href="{{url('/')}}" class="btn btn-info ml-3">Back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
