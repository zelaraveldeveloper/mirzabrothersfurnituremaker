@extends('frontend.layouts.master')

@section('title', '')

@push('style')
    <link href="{{ asset('css/frontend.css') }}" rel="stylesheet">
@endpush

@section('content')
    <section class="content">
        <div id="banner">
            <div class="row">
                <div class="col-12">
                    <div id="carouselExampleIndicators" class="carousel slide  banner" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach ($banners as $key => $banner)
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach ($banners as $key => $banner)
                                <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                    <img class="d-block w-100" src="{{ $banner->image }}" alt="{{ $banner->image }}">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div id="categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 mt-3">
                        <h3 class="title">Shop By Categories</h3>
                        <hr class="mb-4">
                    </div>
                    <div class="col-12">
                        <div class="owl-carousel owl-theme my-owl-carousel">
                            @foreach ($categories as $cate)
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-text">
                                            <h3 class="card-title">{{ $cate->name }}</h3>
                                            <p>{{ strlen($cate->description) > 72 ? substr($cate->description, 0, 72) . '...' : $cate->description }}</p>
                                        </div>
                                        <img class="card-img" src="{{ $cate->image }}" alt="Category Image">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="product-section new-arrivals">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-head">
                            New Arrivals
                            <a href="{{ route('newArrival') }}" class="btn btn-primary" style="float: right">View All</a>
                        </h2>
                        <div class="new-arrival-carousel owl-carousel owl-theme owl-loaded owl-drag">
                            @foreach ($newProducts as $index => $newProduct)
                                <div class="product-grid latest item">
                                    <div class="product-image new-arrival">
                                        <a class="image" href="{{ url('collection/' . $newProduct->subCategory->name . '/' . $newProduct->name) }}">
                                            <img class="pic-1" src="{{ $newProduct->image }}">
                                        </a>
                                        <div class="product-button-group">
                                            <a href="{{ url('collection/' . $newProduct->subCategory->name . '/' . $newProduct->name) }}" ><i class="fa fa-eye"></i></a>
                                            <a href="" class="add-to-cart" data-productid="{{$newProduct->id}}"><i class="fa fa-shopping-cart"></i></a>
                                            @guest
                                                <a class="" href="#" data-toggle="modal" data-target="#userLogin">
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                            @else
                                                <a class="add-to-wishlist" data-productid="{{$newProduct->id}}">
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                            @endguest
                                        </div>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title">
                                            <a href="{{ url('collection/' . $newProduct->subCategory->name . '/' . $newProduct->name) }}">{{ $newProduct->name }}</a>
                                        </h3>
                                        <div class="price">{{ config('app.currency_symbol') }}: {{ $newProduct->salePrice }}</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-head">
                            Best Seller's Trending Product
                            <a href="{{ route('popularProducts') }}" class="btn btn-primary" style="float: right">View All</a>
                        </h2>
                        <div class="popular-carousel owl-carousel owl-theme owl-loaded owl-drag">
                            @foreach ($popularProducts as $index => $popularProduct)
                                <div class="product-grid popular item">
                                    <div class="product-image new-arrival">
                                        <a class="image" href="{{ url('collection/' . $newProduct->subCategory->name . '/' . $newProduct->name) }}">
                                            <img class="pic-1" src="{{ $popularProduct->image }}">
                                        </a>
                                        <div class="product-button-group">
                                            <a href="{{ url('collection/' . $newProduct->subCategory->name . '/' . $newProduct->name) }}" ><i class="fa fa-eye"></i></a>
                                            <a href="" class="add-to-cart" data-productid="{{$popularProduct->id}}"><i class="fa fa-shopping-cart"></i></a>
                                            @guest
                                                <a class="" href="#" data-toggle="modal" data-target="#userLogin">
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                            @else
                                                <a class="add-to-wishlist" data-productid="{{$popularProduct->id}}">
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                            @endguest
                                        </div>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title">
                                            <a href="{{ url('collection/' . $newProduct->subCategory->name . '/' . $newProduct->name) }}">{{ $popularProduct->name }}</a>
                                        </h3>
                                        <div class="price">{{ config('app.currency_symbol') }}: {{ $popularProduct->salePrice }}</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{asset('js/frontend/frontend.js')}}"></script>
@endpush
