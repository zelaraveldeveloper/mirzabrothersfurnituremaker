<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ config('app.name', 'Admin') }} | Login</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

    <link href="{{ asset('css/auth/auth.css') }}" rel="stylesheet">
</head>

<body class="admin-login-page">
    <div class="loader"></div>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div
                        class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <h2 class="text-center text-white mb-3">Admin Login</h2>
                        <div class="card card-primary">
                            <div class="card-body">
                                <form method="POST" action="{{route('login')}}" >
                                    @csrf
                                    <div class="mb-3">
                                        <label class="text-black">Email</label>
                                        <div class="icon-addon input-group-md">
                                            <input type="email" name="email" placeholder="Enter your email address"
                                                class="form-control br @error('email') is-invalid @enderror"
                                                id="email" value="{{old('email')}}" required>
                                            <small for="email" class="" rel="tooltip" title="email">
                                                <i class="fa fa-envelope-o text-dark" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        @error('email')
                                            <div class="mb-4 font-medium text-sm text-danger">
                                                <small>{{ $message }}</small>
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label class="text-black"> Password</label>
                                        <div class="icon-addon input-group-md">
                                            <input type="password" name="password" placeholder="Enter your password"
                                                class="form-control br @error('password') is-invalid @enderror password"
                                                id="" required>
                                            <small for="password" rel="tooltip" title="password">
                                                <i class="fa fa-eye fa-flip-horizontal text-secondary toggle_pwd" id="" type="button"
                                                    aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        @error('password')
                                            <div class="mb-4 font-medium text-sm text-danger">
                                                <small>{{ $message }}</small>
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="row my-4">
                                        <div class="col-12">
                                            <div class="custom-control custom-checkbox">
                                                <input name="remember" class="custom-control-input custom-control-input-danger form-check-input" type="checkbox" id="remember"  {{ old('remember') ? 'checked' : '' }}>
                                                <label for="remember" class="custom-control-label font-weight-normal text-black">Remember me</label>
                                            </div>
                                        </div>
                                        <div class="col-12 mt-2">
                                            {{-- <a href="" type="button"
                                                class="text-black"> Forgot password?</a> --}}
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit"
                                            class="btn btn-warning font-weight-bold btn-block br py-1">
                                            Sign In
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/auth/auth.js') }}"></script>
</body>
</html>
