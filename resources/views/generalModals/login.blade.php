<div class="modal fade" id="userLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Login here</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  method="POST" action="{{ route('login')}}" >
                @csrf
                    <div class="mb-3">
                        <label class="text-black">Email</label>
                        <div class="icon-addon input-group-md">
                            <input type="email" name="email" placeholder="Enter your email address"
                                class="form-control br @error('email') is-invalid @enderror"
                                id="email" value="{{old('email')}}" required>
                            <small for="email" class="" rel="tooltip" title="email">
                                <i class="fa fa-envelope-o text-dark" aria-hidden="true"></i>
                            </small>
                        </div>

                        @error('email')
                            <div class="mb-4 font-medium text-sm text-danger">
                                <small>{{ $message }}</small>
                            </div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label class="text-black"> Password</label>
                        <div class="icon-addon input-group-md">
                            <input type="password" name="password" placeholder="Enter your password"
                                class="form-control br @error('password') is-invalid @enderror password"
                                id="" required>
                            <small for="password" rel="tooltip" title="password">
                                <i class="fa fa-eye fa-flip-horizontal text-secondary toggle_pwd" id="" type="button"
                                    aria-hidden="true"></i>
                            </small>
                        </div>

                        @error('password')
                            <div class="mb-4 font-medium text-sm text-danger">
                                <small>{{ $message }}</small>
                            </div>
                        @enderror
                    </div>

                    <div class="row my-4">
                        <div class="col-12">
                            <div class="custom-control custom-checkbox">
                                <input name="remember" class="custom-control-input custom-control-input-danger form-check-input" type="checkbox" id="remember"  {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember" class="custom-control-label font-weight-normal text-black">Remember me</label>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit"
                            class="btn btn-warning font-weight-bold btn-block br py-1">
                            Sign In
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

