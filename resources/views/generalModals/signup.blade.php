<div class="modal fade" id="userSignup">
    <div class="modal-dialog modal-dialog-centered modal-lg card-middle user-signup">
        <div class="modal-content">
            <div class="modal-header pl-3 pt-3 pb-2 border-0">
                <h4 class="modal-title signup-title">Signup here</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg width="24" height="23" viewBox="0 2 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 2C6.49 2 2 6.49 2 12C2 17.51 6.49 22 12 22C17.51 22 22 17.51 22 12C22 6.49 17.51 2 12 2ZM15.36 14.3C15.65 14.59 15.65 15.07 15.36 15.36C15.21 15.51 15.02 15.58 14.83 15.58C14.64 15.58 14.45 15.51 14.3 15.36L12 13.06L9.7 15.36C9.55 15.51 9.36 15.58 9.17 15.58C8.98 15.58 8.79 15.51 8.64 15.36C8.35 15.07 8.35 14.59 8.64 14.3L10.94 12L8.64 9.7C8.35 9.41 8.35 8.93 8.64 8.64C8.93 8.35 9.41 8.35 9.7 8.64L12 10.94L14.3 8.64C14.59 8.35 15.07 8.35 15.36 8.64C15.65 8.93 15.65 9.41 15.36 9.7L13.06 12L15.36 14.3Z" fill="#445260"/>
                    </svg>
                </button>
            </div>
            <form method="POST" action="{{ route('signUp')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body pb-1">
                    <div class="d-flex">
                        <div class="mb-4" id="avatar">
                            <img id="imagePreview" src="img/default-user-avatar.png"  alt='Student Avatar' style="width: 80px; height: 80px; border-radius: 8px;">
                        </div>
                        <div class="mt-4 ml-3">
                            <input type="file" id="imageInput" name="image" accept="image/*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input  name="fullName" type="text" class="form-control" placeholder="Full Name" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Mobile</label>
                                <input  name="mobile" type="tel" class="form-control" placeholder="Mobile" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Email</label>
                                <div class="icon-addon input-group-md">
                                    <input type="email" name="email" placeholder="Enter your email address"
                                        class="form-control br @error('email') is-invalid @enderror"
                                        id="email" value="{{old('email')}}" required>
                                    <small for="email" class="" rel="tooltip" title="email">
                                        <i class="fa fa-envelope-o text-dark" aria-hidden="true"></i>
                                    </small>
                                </div>

                                @error('email')
                                    <div class="mb-4 font-medium text-sm text-danger">
                                        <small>{{ $message }}</small>
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Password</label>
                                <div class="icon-addon input-group-md">
                                    <input type="password" name="password" placeholder="Enter your password"
                                        class="form-control br @error('password') is-invalid @enderror password"
                                        id="" required>
                                    <small for="password" rel="tooltip" title="password">
                                        <i class="fa fa-eye fa-flip-horizontal text-secondary toggle_pwd" id="" type="button"
                                            aria-hidden="true"></i>
                                    </small>
                                </div>

                                @error('password')
                                    <div class="mb-4 font-medium text-sm text-danger">
                                        <small>{{ $message }}</small>
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Country</label>
                                <select name="countryId" id="country" class="form-control select2" required>
                                    <option value="" selected disabled>Select Country</option>
                                    @foreach ($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label >State</label>
                                <select name="stateId" id="state" class="form-control select2" required>
                                    <option value='' selected disabled>Select State</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>City</label>
                                <input name="city" type="text" class="form-control" placeholder="City" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Address</label>
                                <input name="address" type="text" class="form-control" placeholder="Address" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters p-3 bottom-buttons">
                    <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn  signup-button">SignUp</button>
                </div>
            </form>
        </div>
    </div>
</div>
