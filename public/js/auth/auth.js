$(function () {
    $(".toggle_pwd").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var type = $(this).hasClass("fa-eye-slash") ? "text" : "password";
        $(".password").attr("type", type);
    });
});

