$(document).ready(function(){
    $(".my-owl-carousel").owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1,
                nav: true

            },
            600: {
                items: 2,
                nav: true
            },
            800: {
                items: 3,
                nav: true
            },
            1000: {
                items: 4,
                nav: true,
                loop: false,
                margin: 10
            }
        }
    });

    $('.new-arrival-carousel').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1,
                nav: true

            },
            600: {
                items: 2,
                nav: true
            },
            800: {
                items: 4,
                nav: true
            },
            1000: {
                items: 5,
                nav: true,
                loop: false,
                margin: 10
            }
        }
    });

    $('.popular-carousel').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1,
                nav: true

            },
            600: {
                items: 2,
                nav: true
            },
            800: {
                items: 3,
                nav: true
            },
            1000: {
                items: 4,
                nav: true,
                loop: false,
                margin: 5
            }
        }
    });
});

