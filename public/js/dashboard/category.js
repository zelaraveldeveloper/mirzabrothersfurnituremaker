$(document).ready(function () {
    $('#storeCategoryForm').submit(function (e) {
        e.preventDefault();

        var formData = $(this).serialize();

        $.ajax({
            type: 'POST',
            url: 'category/store',
            data: formData,
            success: function (response) {
                if (response.success == true) {
                    toastr.success(response.message);
                    location.reload();
                }else{
                    toastr.error(response.message)
                }
            },
        });
    });

    $('.editCategory').on('click', function (e) {
        e.preventDefault();

        var categoryId = $(this).data('category-id');

        $.ajax({
            type: 'GET',
            url: '/category/edit/' + categoryId,
            success: function (response) {
                var category = response.category;
                $("#name").val(category.name);
                $('#editCategory').modal('show');
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });

    $('#updateCategoryForm').submit(function (e) {
        e.preventDefault();

        var formData = $(this).serialize();
        var categoryId = $(this).data('category-id');

        $.ajax({
            type: 'POST',
            url: 'category/update/'+ categoryId,
            data: formData,
            success: function (response) {
                if (response.success == true) {
                    toastr.success(response.message);
                    location.reload();
                }else{
                    toastr.error(response.message)
                }
            },
        });
    });
});
