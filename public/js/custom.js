$(function () {
    $(".toggle_pwd").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var type = $(this).hasClass("fa-eye-slash") ? "text" : "password";
        $(".password").attr("type", type);
    });
});


$(document).ready(function() {
    $(".add-to-wishlist").click(function(event) {
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var productId = $(this).data("productid");
        $.ajax({
            type: "POST",
            url: "/wishlist",
            data: { productId: productId },
            success: function(response) {
                alertify.set('notifier','position','top-right');
                alertify.success(response.status);
                countWishlist();
            }
        });
    });

    function countWishlist() {
        $.ajax({
            type: "GET",
            url: "/wishlist/count",
            success: function(response) {
                var wishlistCount = response.count;
                var wishlist = '<span>' + wishlistCount + '</span>';
                $("#wishlist-count").html(wishlist);

            },
            error: function(error) {
                console.error("Error fetching wishlist count:", error);
            }
        });
    }

    $('.remove-wishlist').click(function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var Clickedthis = $(this);
        var wishlistId = $(Clickedthis).closest('.wishlist-content').find('.wishlistId').val();

        $.ajax({
            method: 'POST',
            url: '/removeWishlist',
            data: {
                'wishlistId': wishlistId,
            },
            success: function (response) {
                $(Clickedthis).closest('.wishlist-content').remove();
                alertify.set('notifier','position','top-right');
                alertify.success(response.status);
                countWishlist();
            }
        });
    });

    $('.add-to-cart').click(function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var productId = $(this).data("productid");
        var quantity = $(this).data("qty-input");
        $.ajax({
            url: "/add-to-cart",
            method: "POST",
            data: {
                'quantity': quantity,
                'productId': productId,
            },
            success: function (response) {
                alertify.set('notifier','position','top-right');
                alertify.success(response.status);
                countCart();
            },
        });
    });

    function countCart()
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/cart/count',
            method: "GET",
            success: function (response) {
                var cartCount = response.count;
                var cart = '<span>' + cartCount + '</span>';
                $("#cart-count").html(cart);
            }
        });
    }

    $('.increment-btn').click(function (e) {
        e.preventDefault();
        var incre_value = $(this).parents('.quantity').find('.qty-input').val();
        var value = parseInt(incre_value, 10);
        value = isNaN(value) ? 0 : value;
        if (value < 10) {
            value++;
            $(this).parents('.quantity').find('.qty-input').val(value);
        }

    });

    $('.decrement-btn').click(function (e) {
        e.preventDefault();
        var decre_value = $(this).parents('.quantity').find('.qty-input').val();
        var value = parseInt(decre_value, 10);
        value = isNaN(value) ? 0 : value;
        if(value>1){
            value--;
            $(this).parents('.quantity').find('.qty-input').val(value);
        }
    });

    $('.changeQuantity').click(function (e) {
        e.preventDefault();
        var thisClick = $(this);
        var quantity = $(this).closest(".cartpage").find('.qty-input').val();
        var productId = $(this).closest(".cartpage").find('.product_id').val();

        var data = {
            '_token': $('input[name=_token]').val(),
            'quantity':quantity,
            'productId':productId,
        };

        $.ajax({
            url: '/cart/update',
            type: 'POST',
            data: data,
            success: function (response) {
                thisClick.closest(".cartpage").find('.cart-grand-total-price').text(response.gtprice);
                $('#totalajaxcall').load(location.href + ' .totalpricingload');
                alertify.set('notifier','position','top-right');
                alertify.success(response.status);
            }
        });
    });

    $('.delete_cart_data').click(function (e) {
        e.preventDefault();

        var thisDeletearea = $(this);
        var productId = $(this).closest(".cartpage").find('.product_id').val();

        var data = {
            '_token': $('input[name=_token]').val(),
            "productId": productId,
        };

        $.ajax({
            url: '/delete-from-cart',
            type: 'DELETE',
            data: data,
            success: function (response) {
                thisDeletearea.closest(".cartpage").remove();
                $('#totalajaxcall').load(location.href + ' .totalpricingload');
                alertify.set('notifier','position','top-right');
                alertify.success(response.status);
            }
        });
    });

    $('.clear_cart').click(function (e) {
        e.preventDefault();

        $.ajax({
            url: '/clear-cart',
            type: 'GET',
            success: function (response) {
                window.location.reload();
                alertify.set('notifier','position','top-right');
                alertify.success(response.status);
            }
        });

    });

    $('.signup-button-click').on("click", function() {
        $("#userLogin").modal('hide');
    });

    $('#imageInput').on('change', function (e) {
        var file = e.target.files[0];

        if (file) {
            var reader = new FileReader();

            reader.onload = function (e) {
            $('#imagePreview').attr('src', e.target.result);
            $('#imagePreview').show();
        };
            reader.readAsDataURL(file);
        } else {
          $('#imagePreview').hide();
        }
    });

    $("#country").on("change", function () {
        var countryId = $(this).val();

        $.ajax({
            url: `/states/${countryId}`,
            type: "GET",
            success: function (response) {
                if (response.success == true) {
                    var options =
                        "<option value='' selected disabled>Select State</option>";
                    $.each(response.states, function (index, state) {
                        options +=
                            '<option value="' +
                            state.id +
                            '">' +
                            state.name +
                            "</option>";
                    });

                    $("#state").html(options);
                }else{
                    toastr.error(response.message)
                }
            },
            error: function (error) {},
        });
    });

    scr = "{{ route('searchproductajax') }}";
    $( "#search_text" ).autocomplete({
        source: function(request, response){
            $.ajax({
                url: scr,
                data: {
                    term: request.term
                },
                dataType: "json",
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
    });

    $(document).on('click', '.ui-menu-item', function () {
        $('#search-form').submit();
    });

});
