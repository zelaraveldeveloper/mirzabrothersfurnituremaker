<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Brand\BrandController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Option\OptionController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\Frontend\WishlistController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Frontend\CollectionController;


Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('new-arrival', [FrontendController::class, 'newArrival'])->name('newArrival');
Route::get('popular-products', [FrontendController::class, 'popularProduct'])->name('popularProducts');
Route::get('all-products', [FrontendController::class, 'allProducts'])->name('allProducts');

// Route::post('review/store/{id}',[FrontendController::class,'review'])->name('review.store');

// // Frontend View
Route::get('/searchajax', [CollectionController::class, 'SearchautoComplete'])->name('searchproductajax');
Route::post('/searching', [FrontendController::class, 'result']);

Route::get('collection/{subCategoryUrl}', [FrontendController::class, 'subCategoryView']);
Route::get('collection/{subCategoryUrl}/{productUrl}', [FrontendController::class, 'productView']);

Route::post('add-to-cart', [CartController::class, 'addToCart']);
Route::get('cart', [CartController::class, 'index'])->name('userCart');
Route::get('cart/count', [CartController::class, 'countCart']);
Route::post('cart/update', [CartController::class, 'updateCart']);
Route::delete('delete-from-cart', [CartController::class, 'deletefromcart']);
Route::get('clear-cart', [CartController::class, 'clearcart']);

// Route::get('/thank-you',[CartController::class,'thankyou']);

Route::get('wishlist', [WishlistController::class, 'index'])->name('userWishlist');

Route::get('/states/{id}', [AuthController::class, 'states'])->name('states');
Route::post('signUp', [AuthController::class, 'signUp'])->name('signUp');

Route::middleware(['auth', 'user'])->group(function () {
    Route::get('/home', [FrontendController::class, 'index'])->name('userHome');
    Route::get('logout_user', [FrontendController::class, 'logout_user'])->name('logout_user');

    Route::get('user/profile', [FrontendController::class, 'profile'])->name('userProfile');
    Route::post('user/profile', [FrontendController::class, 'profileUpdate'])->name('profileUpdate');

    Route::post('wishlist', [WishlistController::class, 'addWishlist']);
    Route::get('/wishlist/count', [WishlistController::class, 'countWishlist']);
    Route::post('removeWishlist', [WishlistController::class, 'removeWishlist']);

    Route::get('checkout', [CheckoutController::class, 'index']);
    //     Route::post('place-order',[CheckoutController::class,'storeorder']);
    //     // Coupon Code
    //     Route::post('check-coupon-code',[CheckoutController::class,'checkingcoupon']);
    //     // Razorpay
    //     Route::post('confirm-razorpay-payment',[CheckoutController::class,'checkamount']);

});

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'login'])->name('login');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('changePassword', [AuthController::class, 'changePassword'])->name('changePassword');
    Route::post('updatePassword', [AuthController::class, 'updatePassword'])->name('updatePassword');
    // Route::post('profile/update',[AdminController::class,'profileupdate'])->name('admin.profile.update');

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', [App\Http\Controllers\Category\CategoryController::class, 'index'])->name('categories');
        Route::post('/store', [App\Http\Controllers\Category\CategoryController::class, 'store'])->name('categoryStore');
        Route::get('/edit/{id}', [App\Http\Controllers\Category\CategoryController::class, 'edit'])->name('categoryEdit');
        Route::post('/update/{id}', [App\Http\Controllers\Category\CategoryController::class, 'update'])->name('categoryUpdate');
        Route::get('/delete/{id}', [App\Http\Controllers\Category\CategoryController::class, 'destroy'])->name('categoryDelete');
    });

    Route::group(['prefix' => 'subCategory'], function () {
        Route::get('/', [App\Http\Controllers\SubCategory\SubCategoryController::class, 'index'])->name('subCategories');
        Route::get('/create', [App\Http\Controllers\SubCategory\SubCategoryController::class, 'create'])->name('subCategoryCreate');
        Route::post('/store', [App\Http\Controllers\SubCategory\SubCategoryController::class, 'store'])->name('subCategoryStore');
        Route::get('/edit/{id}', [App\Http\Controllers\SubCategory\SubCategoryController::class, 'edit'])->name('subCategoryEdit');
        Route::post('/update/{id}', [App\Http\Controllers\SubCategory\SubCategoryController::class, 'update'])->name('subCategoryUpdate');
        Route::get('/delete/{id}', [App\Http\Controllers\SubCategory\SubCategoryController::class, 'destroy'])->name('subCategoryDelete');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', [App\Http\Controllers\Product\ProductController::class, 'index'])->name('products');
        Route::get('/create', [App\Http\Controllers\Product\ProductController::class, 'create'])->name('productCreate');
        Route::post('/store', [App\Http\Controllers\Product\ProductController::class, 'store'])->name('productStore');
        Route::get('/edit/{id}', [App\Http\Controllers\Product\ProductController::class, 'edit'])->name('productEdit');
        Route::post('/update/{id}', [App\Http\Controllers\Product\ProductController::class, 'update'])->name('productUpdate');
        Route::get('/delete/{id}', [App\Http\Controllers\Product\ProductController::class, 'destroy'])->name('productDelete');
    });

    Route::group(['prefix' => 'brand'], function () {
        Route::get('/', [BrandController::class, 'index'])->name('brands');
        Route::get('/create', [BrandController::class, 'create'])->name('brandCreate');
        Route::post('/store', [BrandController::class, 'store'])->name('brandStore');
        Route::get('/edit/{id}', [BrandController::class, 'edit'])->name('brandEdit');
        Route::post('/update/{id}', [BrandController::class, 'update'])->name('brandUpdate');
        Route::get('/delete/{id}', [BrandController::class, 'destroy'])->name('brandDelete');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index'])->name('users');
        Route::get('/create', [UserController::class, 'create'])->name('userCreate');
        Route::post('/store', [UserController::class, 'store'])->name('userStore');
        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('userEdit');
        Route::post('/update/{id}', [UserController::class, 'update'])->name('userUpdate');
        Route::get('/show/{id}', [UserController::class, 'show'])->name('userShow');
        Route::get('/delete/{id}', [UserController::class, 'destroy'])->name('userDelete');
    });

    Route::group(['prefix' => 'option'], function () {
        Route::get('/', [OptionController::class, 'index'])->name('options');
        Route::post('update/{id}', [OptionController::class, 'update'])->name('optionUpdate');
    });

    Route::group(['prefix' => 'banner'], function () {
        Route::get('/', [App\Http\Controllers\Banner\BannerController::class, 'index'])->name('banners');
        Route::get('/create', [App\Http\Controllers\Banner\BannerController::class, 'create'])->name('bannerCreate');
        Route::post('/store', [App\Http\Controllers\Banner\BannerController::class, 'store'])->name('bannerStore');
        Route::get('/edit/{id}', [App\Http\Controllers\Banner\BannerController::class, 'edit'])->name('bannerEdit');
        Route::post('/update/{id}', [App\Http\Controllers\Banner\BannerController::class, 'update'])->name('bannerUpdate');
        Route::get('/delete/{id}', [App\Http\Controllers\Banner\BannerController::class, 'destroy'])->name('bannerDelete');
    });

    // Route::get('orders',[OrderController::class,'index'])->name('admin.orders');
    // Route::get('order-view/{order_id}',[OrderController::class,'vieworder']);
    // Route::get('order-proceed/{order_id}',[OrderController::class,'proceedorder']);
    // Route::post('order/update-tracking-status/{order_id}',[OrderController::class,'trackingstatus']);
    // Route::post('order/cancel-order/{order_id}',[OrderController::class,'cancelorder']);
    // Route::put('order/complete-order/{order_id}',[OrderController::class,'completeorder']);
    // Route::get('generate-invoice/{order_id}',[OrderController::class,'invoice']);


    // Route::get('coupons',[CouponController::class,'index'])->name('admin.coupons');
    // Route::post('coupon-store',[CouponController::class,'store']);
    // Route::get('admin/coupon-edit/{id}',[CouponController::class,'edit']);
    // Route::post('coupon-update/{id}',[CouponController::class,'update']);
});
