<?php

namespace Database\Seeders;

use App\Models\Option;
use Illuminate\Database\Seeder;
use App\Traits\CheckExistingEntriesTrait;

class OptionSeeder extends Seeder
{
    use CheckExistingEntriesTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->entriesExist(Option::class)) {
            return;
        }

        Option::insert([
            'title' => 'SUPER MARKET',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, perspiciatis quia repudiandae sapiente sed sunt.',
            'email' => 'email@gmail.com',
            'phone' => '9876541230',
            'image' => 'default-avatar.png',
            'footerText' => 'Copyright ©',
            'currency' => 'Rs',
            'address' => '#123, Lorem Ipsum',
            'facebook' => 'https://web.facebook.com/',
            'instagram' => 'https://www.instagram.com/',
        ]);
    }
}
