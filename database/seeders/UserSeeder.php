<?php

namespace Database\Seeders;

use App\Models\User;
use App\Enums\UserRoleEnum;
use App\Enums\UserStatusEnum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Traits\CheckExistingEntriesTrait;

class UserSeeder extends Seeder
{
    use CheckExistingEntriesTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->entriesExist(User::class)) {
            return;
        }

        User::insert([
            'fullName' => 'John Deo',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'image' => null,
            'mobile' => '00000000',
            'countryId' => '166',
            'stateId' => '2728',
            'city' => 'Lahore',
            'address' => 'Gulberg Lahore',
            'role' => UserRoleEnum::ADMIN,
            'status' => UserStatusEnum::APPROVED,
        ]);
    }
}
