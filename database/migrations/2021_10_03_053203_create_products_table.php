<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('userId');
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('categoryId');
            $table->foreign('categoryId')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('subCategoryId');
            $table->foreign('subCategoryId')->references('id')->on('subCategories')->onDelete('cascade');
            $table->unsignedBigInteger('brandId')->nullable();
            $table->foreign('brandId')->references('id')->on('brands')->onDelete('cascade');

            $table->string('name');
            $table->mediumText('smallDescription');
            $table->string('image');

            $table->integer('originalPrice');
            $table->integer('offerPrice');
            $table->integer('salePrice');
            $table->integer('quantity');

            $table->tinyInteger('isNew')->default(false);
            $table->tinyInteger('isPopular')->default(false);
            $table->enum('status', ['ACTIVE', 'INACTIVE'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
