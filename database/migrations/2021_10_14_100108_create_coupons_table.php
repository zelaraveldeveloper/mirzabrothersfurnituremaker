<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('offerName');
            $table->string('productId')->default('0');
            $table->string('couponCode');
            $table->string('couponLimit');
            $table->string('couponType');
            $table->string('couponPrice');
            $table->dateTime('startDatetime');
            $table->dateTime('endDatetime');
            $table->enum('status', ['ACTIVE', 'INACTIVE'])->nullable();
            $table->enum('visibilityStatus', ['ACTIVE', 'INACTIVE'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
