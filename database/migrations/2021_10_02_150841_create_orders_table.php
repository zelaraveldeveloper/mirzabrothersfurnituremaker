<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('userId');
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->string('trackingNo');
            $table->string('trackingMsg')->nullable();
            $table->string('paymentId')->nullable();
            $table->tinyInteger('paymentStatus')->default('0');
            $table->enum('paymentMode', ['PAYPAL', 'STRIPE', 'CARD', 'CASH_ON_DELIVERY']);
            $table->enum('orderStatus', ['ACCEPTED', 'ONGOING', 'CANCELED', 'COMPLETED'])->nullable();
            $table->string('cancelReason')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
