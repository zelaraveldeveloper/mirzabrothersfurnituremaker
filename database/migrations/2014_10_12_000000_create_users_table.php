<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('fullName');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('image')->nullable();
            $table->string('mobile')->nullable();
            $table->string('countryId')->nullable();
            $table->string('stateId')->nullable();
            $table->string('city')->nullable();
            $table->string('address');
            $table->enum('role', ['ADMIN', 'USER'])->nullable();
            $table->enum('status', ['REQUESTED', 'APPROVED', 'REJECTED', 'BLOCKED'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
