<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

/**
 * @method static static APPROVED()
 * @method static static REQUESTED()
 * @method static static REJECTED()
 * @method static static BLOCKED()
 */
final class UserStatusEnum extends Enum
{
    const APPROVED  =   'APPROVED';
    const REQUESTED =   'REQUESTED';
    const REJECTED  =   'REJECTED';
    const BLOCKED   =   'BLOCKED';
}
