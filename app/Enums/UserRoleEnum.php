<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

/**
 * @method static static ADMIN()
 * @method static static USER()
 */
final class UserRoleEnum extends Enum
{
    const ADMIN = 'ADMIN';
    const USER = 'USER';
}
