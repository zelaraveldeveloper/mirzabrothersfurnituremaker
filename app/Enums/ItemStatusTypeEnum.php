<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

/**
 * @method static static ACTIVE()
 * @method static static INACTIVE()
 */
final class ItemStatusTypeEnum extends Enum
{
    const ACTIVE  =   'ACTIVE';
    const INACTIVE =   'INACTIVE';
}
