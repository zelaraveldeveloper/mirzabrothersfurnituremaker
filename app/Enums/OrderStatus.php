<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;


final class OrderStatus extends Enum
{
    const ACCEPTED = "ACCEPTED";
    const ONGOING  ="ONGOING";
    const CANCELED  = "CANCELED";
    const COMPLETED  = "COMPLETED";
}
