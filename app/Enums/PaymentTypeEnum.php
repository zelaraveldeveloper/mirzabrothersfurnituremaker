<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

/**
 * @method static static PAYPAL()
 * @method static static STRIPE()
 * @method static static CARD()
 * @method static static CASH_ON_DELIVERY()
 */
final class PaymentTypeEnum extends Enum
{
    const PAYPAL =   "PAYPAL";
    const STRIPE =   "STRIPE";
    const CARD =   "CARD";
    const CASH_ON_DELIVERY =   "CASH_ON_DELIVERY";
}
