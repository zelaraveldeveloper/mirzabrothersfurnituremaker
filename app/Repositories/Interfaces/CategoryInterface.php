<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Interfaces;


interface CategoryInterface
{

    /**
     * Description - Category store
     * @author Zeeshan N
     */
    public function store($data);

    /**
     * Description - Category edit
     * @author Zeeshan N
     */
    public function editCategory($id);

    /**
     * Description - Category update
     * @author Zeeshan N
     */
    public function update($data, $id);

    /**
     * Description - Category delete
     * @author Zeeshan N
     */
    public function destroy($id);
}
