<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Interfaces;


interface BrandInterface
{

    /**
     * Description - Brand store
     * @author Zeeshan N
     */
    public function store($data);

    /**
     * Description - Brand update
     * @author Zeeshan N
     */
    public function update($data, $id);

    /**
     * Description - Brand delete
     * @author Zeeshan N
     */
    public function destroy($id);
}
