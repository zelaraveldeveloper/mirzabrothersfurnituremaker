<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Interfaces;


interface UserInterface
{

    /**
     * Description - User store
     * @author Zeeshan N
     */
    public function store($data);

    /**
     * Description - User update
     * @author Zeeshan N
     */
    public function update($data, $id);

    /**
     * Description - User delete
     * @author Zeeshan N
     */
    public function destroy($id);
}
