<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Interfaces;


interface ProductInterface
{

    /**
     * Description - Product store
     * @author Zeeshan N
     */
    public function store($data, $isNew, $isPopular);

    /**
     * Description - Product update
     * @author Zeeshan N
     */
    public function update($data, $isNew, $isPopular, $id);

    /**
     * Description - Product delete
     * @author Zeeshan N
     */
    public function destroy($id);
}
