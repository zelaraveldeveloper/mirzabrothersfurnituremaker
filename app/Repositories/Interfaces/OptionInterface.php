<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Interfaces;


interface OptionInterface
{
    /**
     * Description - Option update
     * @author Zeeshan N
     */
    public function update($data, $id);
}
