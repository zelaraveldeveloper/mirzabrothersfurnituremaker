<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Interfaces;


interface BannerInterface
{

    /**
     * Description - Banner store
     * @author Zeeshan N
     */
    public function store($data);

    /**
     * Description - Banner update
     * @author Zeeshan N
     */
    public function update($data, $id);

    /**
     * Description - Banner delete
     * @author Zeeshan N
     */
    public function destroy($id);
}
