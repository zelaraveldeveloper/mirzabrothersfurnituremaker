<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Interfaces;


interface SubCategoryInterface
{

    /**
     * Description - SubCategory store
     * @author Zeeshan N
     */
    public function store($data);

    /**
     * Description - SubCategory update
     * @author Zeeshan N
     */
    public function update($data, $id);

    /**
     * Description - SubCategory delete
     * @author Zeeshan N
     */
    public function destroy($id);
}
