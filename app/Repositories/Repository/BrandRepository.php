<?php

/**
 * @author Zeeshan N
 */

namespace App\Repositories\Repository;

use Exception;
use App\Models\Brand;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Interfaces\BrandInterface;
use App\Repositories\Repository\General\GeneralRepository;

class BrandRepository implements BrandInterface
{
    public $generalRepo;

    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    /**
     * Description - Brand store
     * @author Zeeshan N
     */
    public function store($data)
    {
        try {
            DB::beginTransaction();

            $brand = new Brand();
            $brand->userId = Auth::id();
            $brand = $this->generalRepo->save($brand, $data);

            DB::commit();
            return $brand;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Description - Brand update
     * @author Zeeshan N
     */
    public function update($data, $id)
    {
        try {
            DB::beginTransaction();

            $brand = Brand::where('id', $id)->first();
            $brand->userId = Auth::id();
            $brand = $this->generalRepo->save($brand, $data);

            DB::commit();
            return $brand;
    } catch (Exception $e) {
        DB::rollBack();
        throw new Exception($e->getMessage());
    }
    }

    /**
     * Description - Brand delete
     * @author Zeeshan N
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $brand = Brand::find($id);
            $brand->delete();

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }
}
