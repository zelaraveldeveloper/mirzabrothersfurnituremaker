<?php

namespace App\Repositories\Repository\General;
use Illuminate\Support\Str;

class GeneralRepository
{
    public function __construct()
    {
    }

    public function save($model, $data)
    {
        $model->fill($data);
        if ($model->save()) {
            return $model;
        }
        return false;
    }

    public function uploadImage($file, $path)
    {
        if ($file) {
            $fileName = Str::random(10) . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path($path);
            $file->move($destinationPath, $fileName);
            return  $fileName;
        } else {
            return FALSE;
        }

    }
}
