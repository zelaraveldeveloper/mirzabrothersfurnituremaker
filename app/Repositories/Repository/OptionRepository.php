<?php

namespace App\Repositories\Repository;

use App\Models\Option;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Repositories\Interfaces\OptionInterface;
use App\Repositories\Repository\General\GeneralRepository;
use Exception;

class OptionRepository implements OptionInterface
{
    public $generalRepo;
    /**
     * Constructor function.
     */
    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    /**
     * Option update
     */
    public function update($data, $id)
    {
        try {
            DB::beginTransaction();
            $option = Option::where('id',$id)->first();
            if (isset($data['image'])) {
                $dest = $option->image;

                if (File::exists($dest)) {
                    File::delete($dest);
                }
                $option->image = $this->generalRepo->uploadImage($data['image'], 'upload/web/images');
            }
            $option = $this->generalRepo->save($option, $data);

            DB::commit();
            return $option;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

}
