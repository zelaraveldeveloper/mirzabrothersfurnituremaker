<?php

namespace App\Repositories\Repository;

use App\Models\Banner;
use Illuminate\Support\Facades\File;
use App\Repositories\Interfaces\BannerInterface;
use App\Repositories\Repository\General\GeneralRepository;

class BannerRepository implements BannerInterface
{
    public $generalRepo;

    /**
     * Constructor function.
     */
    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    /**
     * Banner store
     */
    public function store($data)
    {
        $banner = new Banner();
        if (isset($data['image'])) {
            $banner->image = $this->generalRepo->uploadImage($data['image'], 'upload/banner/images');
        }
        return $this->generalRepo->save($banner, $data);
    }

    /**
     * Banner update
     */
    public function update($data, $id)
    {
        $banner = Banner::where('id',$id)->first();
        if (isset($data['image'])) {
            $dest = $banner->image;

            if (File::exists($dest)) {
                File::delete($dest);
            }
            $banner->image = $this->generalRepo->uploadImage($data['image'], 'upload/banner/images');
        }
        return $this->generalRepo->save($banner, $data);
    }

    /**
     * Banner delete
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        return $banner->delete();
    }
}
