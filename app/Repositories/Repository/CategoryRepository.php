<?php

namespace App\Repositories\Repository;

use Exception;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Repositories\Interfaces\CategoryInterface;
use App\Repositories\Repository\General\GeneralRepository;

class CategoryRepository implements CategoryInterface
{
    public $generalRepo;
    /**
     * Constructor function.
     */
    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    /**
     * Category store
     */
    public function store($data)
    {
        try {
            DB::beginTransaction();

            $category = new Category();
            $category->userId = Auth::id();
            $category = $this->generalRepo->save($category, $data);

            DB::commit();
            return $category;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Category edit
     */
    public function editCategory($id)
    {
        try {
            return Category::findorFail($id);
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Category update
     */
    public function update($data, $id)
    {
        try {
            DB::beginTransaction();
            $category = Category::where('id',$id)->first();
            $category->userId = Auth::id();
            $$category = $this->generalRepo->save($category, $data);

            DB::commit();
            return $category;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Category delete
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $category = Category::find($id);
            $category->delete();

            DB::commit();
            return true;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }
}
