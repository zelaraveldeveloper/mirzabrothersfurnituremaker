<?php

namespace App\Repositories\Repository;

use Exception;
use App\Models\SubCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Repositories\Interfaces\SubCategoryInterface;
use App\Repositories\Repository\General\GeneralRepository;

class SubCategoryRepository implements SubCategoryInterface
{
    public $generalRepo;
    /**
     * Constructor function.
     */
    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    /**
     * SubCategory store
     */
    public function store($data)
    {
        try {
            DB::beginTransaction();
            $subCategory = new SubCategory();
            $subCategory->userId = Auth::id();
            $subCategory->categoryId = $data['categoryId'];
            $subCategory = $this->generalRepo->save($subCategory, $data);

            DB::commit();
            return $subCategory;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * SubCategory update
     */
    public function update($data, $id)
    {
        try {
            DB::beginTransaction();
            $subCategory = SubCategory::where('id', $id)->first();
            $subCategory->userId = Auth::id();
            $subCategory->categoryId = $data['categoryId'];
            $subCategory = $this->generalRepo->save($subCategory, $data);

            DB::commit();
            return $subCategory;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * SubCategory delete
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $subCategory = SubCategory::find($id);
            $subCategory->delete();

            DB::commit();
            return true;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }
}
