<?php

namespace App\Repositories\Repository;

use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Repository\General\GeneralRepository;

class UserRepository implements UserInterface
{
    public $generalRepo;
    /**
     * Constructor function.
     */
    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    /**
     * User store
     */
    public function store($data)
    {
        $user = new User();
        $user->password = Hash::make($data['password']);
        if (isset($data['image'])) {
            $user->image = $this->generalRepo->uploadImage($data['image'], 'upload/user/images');
        }
        return $this->generalRepo->save($user, $data);
    }

    /**
     * SubCategory update
     */
    public function update($data, $id)
    {
        $user = User::where('id',$id)->first();
        if (isset($data['image'])) {
            $dest = $user->image;

            if (File::exists($dest)) {
                File::delete($dest);
            }
            $user->image = $this->generalRepo->uploadImage($data['image'], 'upload/user/images');
        }
        return $this->generalRepo->save($user, $data);
    }

    /**
     * User delete
     */
    public function destroy($id)
    {
        $user = User::find($id);
        return $user->delete();
    }
}
// {
//     public $baseRepo, $user_token, $seller, $user;

//     public function __construct()
//     {
//         $this->baseRepo = new BaseRepository();
//         $this->user = new User();
//     }

//     public function getProfile($id)
//     {
//         $user = $this->user->findorfail($id);
//         return $user;
//     }

//     public function profileDelete($id)
//     {
//         $user = $this->user->findorfail($id);
//         return $user->delete();
//     }

//     public function profileCreate(array $data)
//     {
//             if (isset($data['picture'])) {
//                 $fileName = $this->baseRepo->uploadImage($data['picture'], '/upload/user/picture');
//             }

//             $user = $this->user->newInstance();
//             $user->password = Hash::make($data['password']);
//             $user->picture = $fileName;
//             return $this->baseRepo->save($user, $data);
//     }

//     public function updateProfile(array $data)
//     {
//             if (isset($data['picture'])) {
//                 $fileName = $this->baseRepo->uploadImage($data['picture'], '/upload/user/picture');
//             }

//             $user = $this->user::where('id',$data['profileId'])->first();
//             $user->password = Hash::make($data['password']);
//             $user->picture = $fileName;
//             return $this->baseRepo->save($user, $data);
//     }

//     public function adminProfileCreate(array $data)
//     {
//         if (isset($data['picture'])) {
//             $fileName = $this->baseRepo->uploadImage($data['picture'], '/upload/user/picture');
//         }
//         $user = $this->user->newInstance();
//         $user->password = Hash::make($data['password']);
//         $user->picture = $fileName;
//         return $this->baseRepo->save($user, $data);
//     }

//     public function adminProfileupdate(array $data, $id)
//     {
//         if (isset($data['picture'])) {
//             $fileName = $this->baseRepo->uploadImage($data['picture'], '/upload/user/picture');
//         }
//         $user = $this->user::where('id',$id)->first();
//         $user->picture = $fileName;
//         return $this->baseRepo->save($user, $data);
//     }

//     public function adminProfileEdit($id)
//     {
//         $user = $this->user->findorfail($id);
//         return $user;
//     }

//     public function adminProfileDelete($id)
//     {
//         $user = $this->user->findorfail($id);
//         return $user->delete();
//     }
// }
