<?php

namespace App\Repositories\Repository;

use Exception;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Repositories\Interfaces\ProductInterface;
use App\Repositories\Repository\General\GeneralRepository;

class ProductRepository implements ProductInterface
{
    public $generalRepo;
    /**
     * Constructor function.
     */
    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    /**
     * Product store
     */
    public function store($data, $isNew, $isPopular)
    {
        try {
            DB::beginTransaction();

            $product = new Product();
            $product->userId = Auth::id();
            if (isset($data['image'])) {
                $product->image = $this->generalRepo->uploadImage($data['image'], 'upload/product/images');
            }
            $product->isNew = $isNew;
            $product->isPopular = $isPopular;
            $product = $this->generalRepo->save($product, $data);

            DB::commit();
            return $product;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Product update
     */
    public function update($data, $isNew,  $isPopular, $id)
    {
        try {
            DB::beginTransaction();

            $product = Product::where('id', $id)->first();
            $product->userId = Auth::id();

            if (isset($data['image'])) {
                $dest = $product->image;

                if (File::exists($dest)) {
                    File::delete($dest);
                }
                $product->isNew = $isNew;
                $product->isPopular = $isPopular;
                $product->image = $this->generalRepo->uploadImage($data['image'], 'upload/product/images');
            }
            $product = $this->generalRepo->save($product, $data);
            DB::commit();
            return $product;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Product delete
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $product = Product::find($id);
            $product->delete();

            DB::commit();
            return true;
        }catch(Exception $e){
            DB::rollBack();
            throw new Exception($e->getMessage());
        }
    }
}
