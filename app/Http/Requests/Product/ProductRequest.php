<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subCategoryId' => 'required|exists:subcategories,id',
            'categoryId' => 'required|exists:categories,id',
            'brandId' => 'sometimes',
            'name' => 'required|unique:products,name',
            'smallDescription' => 'required',
            'originalPrice' => 'required',
            'offerPrice' => 'required',
            'salePrice' => 'required',
            'isPopular' => 'sometimes',
            'quantity' => 'required',
            'status' => 'sometimes',
            'isNew' => 'sometimes',
            'image' => 'sometimes|file|mimes:jpeg,png,jpg,gif|max:5048',
        ];
    }
}
