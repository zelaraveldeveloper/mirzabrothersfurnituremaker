<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subCategoryId' => 'sometimes',
            'categoryId' => 'sometimes',
            'brandId' => 'sometimes',
            'name' => 'sometimes',
            'smallDescription' => 'sometimes',
            'originalPrice' => 'sometimes',
            'offerPrice' => 'sometimes',
            'salePrice' => 'sometimes',
            'isPopular' => 'sometimes',
            'quantity' => 'sometimes',
            'status' => 'sometimes',
            'isNew' => 'sometimes',
            'image' => 'sometimes|file|mimes:jpeg,png,jpg,gif|max:5048',
        ];
    }
}
