<?php

namespace App\Http\Requests\Option;

use Illuminate\Foundation\Http\FormRequest;

class OptionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes',
            'title' => 'sometimes',
            'description' => 'sometimes',
            'email' => 'sometimes',
            'phone' => 'sometimes',
            'footerText' => 'sometimes',
            'currency' => 'sometimes',
            'address' => 'sometimes',
            'logo' => 'sometimes|file|mimes:jpeg,png,jpg,gif|max:5048',
        ];
    }
}
