<?php

namespace App\Http\Requests\Auth;

use App\Rules\CheckPassword;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currentPassword' => ['required', new CheckPassword],
            'newPassword' => ['required'],
            'newConfirmPassword' => ['same:newPassword'],
        ];
    }
}
