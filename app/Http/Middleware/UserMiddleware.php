<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Enums\UserRoleEnum;
use Illuminate\Http\Request;
use App\Enums\UserStatusEnum;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()){
            if(Auth::user()->role == UserRoleEnum::USER) {
                return $next($request);
            } else {
                return redirect('/');
            }
        }
        return $next($request);
        // if(Auth::check() && Auth::user()->status)
        // {
        //     if (Auth::user()->status == UserStatusEnum::BLOCKED) {
        //         Auth::logout();
        //     }
        //     return redirect()->route('home')
        //                      ->with('status', 'Your Account has Been Banned. Please Contect Administrator.')
        //                      ->withErrors(['email' => 'Your Account has Been Banned. Please Contect Administrator.']);
        // }

        // if(Auth::check())
        // {
        //     $expiresAt = Carbon::now()->addMinutes(1);
        //     Cache::put('user-is-online' . Auth::user()->id, true, $expiresAt);
        // }
        // return $next($request);
    }
}
