<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Enums\UserRoleEnum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()){
            if(Auth::user()->role == UserRoleEnum::ADMIN) {
                return $next($request);
            } else {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
