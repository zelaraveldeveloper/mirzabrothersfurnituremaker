<?php
/**
 * @author Zeeshan N
 */

namespace App\Http\Controllers\Brand;

use Exception;
use App\Models\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\Brand\BrandRequest;
use App\Http\Requests\Brand\BrandUpdateRequest;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Interfaces\BrandInterface;

class BrandController extends Controller
{
    private $brandRepo;

    public function __construct(BrandInterface $brandRepo)
    {
        $this->brandRepo = $brandRepo;
    }

    /**
     * Description - Brands listing
     * @author Zeeshan N
     */
    public function index()
    {
        try {
            $brands = Brand::where('userId', Auth::id())->get();
            return view('backend.brand.index', compact('brands'));

        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Brand create blade
     * @author Zeeshan N
     */
    public function create()
    {
        try {
            return view('backend.brand.create');

        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Brand store
     * @author Zeeshan N
     */
    public function store(BrandRequest $request)
    {
        try {
            $this->brandRepo->store($request->all());
            return redirect()->route('brands')->with('success', 'Data Saved Succesfully');

        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Brand edit
     * @author Zeeshan N
     */
    public function edit($id)
    {
        try {
            $brand = Brand::findorFail($id);
            return view('backend.brand.edit',compact('brand'));
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Brand update
     * @author Zeeshan N
     */
    public function update(BrandUpdateRequest $request, $id)
    {
        try {
            $this->brandRepo->update($request->all(), $id);
            return redirect()->route('brands')->with('success', 'Data Updated Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Brand delete
     * @author Zeeshan N
     */
    public function destroy($id)
    {
        try {
            $this->brandRepo->destroy($id);
            return redirect()->route('brands')->with('success', 'Data Deleted Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }
}
