<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $brands = Brand::where('userId',Auth::id())->count();
        $categories  = Category::where('userId',Auth::id())->count();
        $subCategories  = SubCategory::where('userId',Auth::id())->count();
        $products = Product::where('userId',Auth::id())->count();
        $orders = Order::count();
        $users = User::count();
        return view('backend.dashboard.index',compact('brands','categories','subCategories','products','orders','users'));
    }
}
