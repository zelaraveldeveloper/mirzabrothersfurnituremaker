<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Country;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function addWishlist(Request $request)
    {
        if ( Wishlist::where('userId',Auth::id())->where('productId', $request->productId)->exists() ) {
            return response()->json(['status'=>'Product is already Added to Wishlist']);
        } else {
            $wishlist = new Wishlist();
            $wishlist->userId = Auth::id();
            $wishlist->productId = $request->productId;
            $wishlist->save();
            return response()->json(['status'=>'Product is added to Wishlist']);
        }
    }

    public function countWishlist() {
        $wishlistCount = Wishlist::where('userId', Auth::id())->count();
        return response()->json(['count' => $wishlistCount]);
    }

    public function index()
    {
        $wishlist = Wishlist::where('userId', Auth::id())->get();
        return view('frontend.wishlist.index', compact('wishlist'));
    }


    public function removeWishlist(Request $request)
    {
        if (Wishlist::where('userId', Auth::id())->where('id', $request->wishlistId)->exists()) {
            $wishlist = Wishlist::where('userId', Auth::id())->where('id', $request->wishlistId)->first();
            $wishlist->delete();
            return response()->json(['status'=>'Item Removed from Wishlist']);
        } else {
            return response()->json(['status'=>'No Items Found in Wishlist']);
        }
    }
}
