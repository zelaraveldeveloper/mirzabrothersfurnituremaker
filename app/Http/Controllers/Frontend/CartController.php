<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Country;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{
    public function addtocart(Request $request)
    {
        if (Cookie::get('shopping_cart')) {
            $cookie_data = stripslashes(Cookie::get('shopping_cart'));
            $cart_data = json_decode($cookie_data, true);
        } else {
            $cart_data = array();
        }

        $item_id_list = array_column($cart_data, 'item_id');
        $prod_id_is_there = $request->productId;

        if (in_array($prod_id_is_there, $item_id_list)) {
            foreach ($cart_data as $keys => $values) {

                if ($cart_data[$keys]["item_id"] == $request->productId) {

                    $cart_data[$keys]["item_quantity"] = $request->input('quantity');
                    $item_data = json_encode($cart_data);
                    $minutes = 60;
                    Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                    return response()->json(['status'=>''.$cart_data[$keys]["item_name"].' Already Added to Cart']);
                }
            }
        }
        else
        {
            $product = Product::find($request->productId);
            if ($product) {
                $item_array = array(
                    'item_id' => $request->productId,
                    'item_name' => $product->name,
                    'item_quantity' => $request->quantity ?? 1,
                    'item_price' => $product->salePrice,
                    'item_image' => $product->image
                );
                $cart_data[] = $item_array;

                $item_data = json_encode($cart_data);
                $minutes = 60;
                Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                return response()->json(['status'=> $product->name.' Added to Cart']);
            }
        }
    }

    public function countCart()
    {
        if (Cookie::get('shopping_cart')) {
            $cookie_data = stripslashes(Cookie::get('shopping_cart'));
            $cart_data = json_decode($cookie_data, true);
            $totalcart = count($cart_data);
            return response()->json(['count' => $totalcart]);
        } else {
            $totalcart = "0";
            return response()->json(['count' => $totalcart]);
        }
    }

    public function index()
    {
        $cookie_data = stripslashes(Cookie::get('shopping_cart'));
        $cartData = json_decode($cookie_data, true);
        return view('frontend.cart.index', compact('cartData'));
    }


    public function updateCart(Request $request)
    {
        if(Cookie::get('shopping_cart'))
        {
            $cookie_data = stripslashes(Cookie::get('shopping_cart'));
            $cart_data = json_decode($cookie_data, true);

            $item_id_list = array_column($cart_data, 'item_id');
            $prod_id_is_there = $request->productId;

            if(in_array($prod_id_is_there, $item_id_list))
            {
                foreach($cart_data as $keys => $values)
                {
                    if($cart_data[$keys]["item_id"] == $request->productId)
                    {
                        $cart_data[$keys]["item_quantity"] =  $request->quantity;
                        $ttprice = ($cart_data[$keys]["item_price"] *  $request->quantity);
                        $grandtotalprice = number_format($ttprice);
                        $item_data = json_encode($cart_data);
                        $minutes = 60;
                        Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                        return response()->json([
                            'status'=>'"'.$cart_data[$keys]["item_name"].'" Quantity Updated',
                            'gtprice'=>''.$grandtotalprice.''
                        ]);
                    }
                }
            }
        }
    }

    public function deletefromcart(Request $request)
    {
        $prod_id = $request->input('productId');

        $cookie_data = stripslashes(Cookie::get('shopping_cart'));
        $cart_data = json_decode($cookie_data, true);

        $item_id_list = array_column($cart_data, 'item_id');
        $prod_id_is_there = $prod_id;

        if(in_array($prod_id_is_there, $item_id_list))
        {
            foreach($cart_data as $keys => $values)
            {
                if($cart_data[$keys]["item_id"] == $prod_id)
                {
                    unset($cart_data[$keys]);
                    $item_data = json_encode($cart_data);
                    $minutes = 60;
                    Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                    return response()->json(['status'=>'Item Removed from Cart']);
                }
            }
        }
    }

    public function clearcart()
    {
        Cookie::queue(Cookie::forget('shopping_cart'));
        return response()->json(['status'=>'Your Cart is Cleared']);
    }

    public function thankyou()
    {
        return view('frontend.thank-you');
    }
}
