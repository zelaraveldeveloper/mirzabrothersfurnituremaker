<?php

namespace App\Http\Controllers\Frontend;

use App\Models\User;
use App\Models\Brand;
use App\Models\Banner;
use App\Models\Option;
use App\Models\Country;
use App\Models\Product;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Enums\ItemStatusTypeEnum;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class FrontendController extends Controller
{
    public function index()
    {
        $banners = Banner::where('status', ItemStatusTypeEnum::ACTIVE)->get();
        $categories = Category::get();
        $subCategories = SubCategory::get();
        $newProducts = Product::where('status', ItemStatusTypeEnum::ACTIVE)->where('isNew', '1')->orderBy('id','desc')->get();
        $popularProducts = Product::where('status', ItemStatusTypeEnum::ACTIVE)->where('isPopular', '1')->orderBy('id','desc')->get();
        $option = Option::first();
        $countries = Country::get();
        return view ('frontend.home.index', compact('banners', 'categories', 'subCategories', 'newProducts', 'popularProducts', 'option', 'countries'));
    }

    public function newArrival()
    {
        $newProducts = Product::where('status', ItemStatusTypeEnum::ACTIVE)->where('isNew', '1')->orderBy('created_at', 'desc')->get();
        return view ('frontend.product.newProducts', compact('newProducts'));
    }

    public function popularProduct()
    {
        $popularProducts = Product::where('status', ItemStatusTypeEnum::ACTIVE)->where('isPopular', '1')->orderBy('created_at', 'desc')->get();
        return view ('frontend.product.popularProducts', compact('popularProducts'));
    }

    public function allProducts()
    {
        $allProducts = Product::where('status', ItemStatusTypeEnum::ACTIVE)->orderBy('created_at', 'desc')->get();
        return view('frontend.product.allProducts', compact('allProducts'));
    }

    public function productView($subCategoryUrl, $productUrl)
    {
        $product = Product::where('name', $productUrl)->where('status', ItemStatusTypeEnum::ACTIVE)->first();
        return view('frontend.product.singleProduct', compact('product'));
    }

    public function subCategoryView($subCategoryUrl, Request $request)
    {
        $subCategory = SubCategory::where('name', $subCategoryUrl)->first();
        $brandList = Brand::where('categoryId', $subCategory->categoryId)->get();

        if ($request->get('sort') == 'price_asc') {
            $products = Product::where('subCategoryId', $subCategory->id)->orderBy('salePrice', 'asc')->where('status', ItemStatusTypeEnum::ACTIVE)->get();
        } elseif ($request->get('sort') == 'price_desc') {
            $products = Product::where('subCategoryId', $subCategory->id)->orderBy('salePrice', 'desc')->where('status', ItemStatusTypeEnum::ACTIVE)->get();
        } elseif ($request->get('sort') == 'newest') {
            $products = Product::where('subCategoryId', $subCategory->id)->orderBy('created_at', 'desc')->where('isNew', '1')->where('status', ItemStatusTypeEnum::ACTIVE)->get();
        } elseif ($request->get('sort') == 'popularity') {
            $products = Product::where('subCategoryId', $subCategory->id)->where('isPopular', '1')->where('status', ItemStatusTypeEnum::ACTIVE)->get();
        } elseif ($request->get('filterBrand')) {
            $checked = $_GET['filterBrand'];
            $brandFilter = Brand::whereIn('name',  $checked)->get();
            $brandId = [];

            foreach($brandFilter as $brandFilterList) {
                array_push($brandId, $brandFilterList->id);
            }

            $products = Product::whereIn('brandId', $brandId)->where('status', ItemStatusTypeEnum::ACTIVE)->get();
        } else {
            $products = Product::where('subCategoryId', $subCategory->id)->where('status', ItemStatusTypeEnum::ACTIVE)->get();
        }
        return view('frontend.product.products', compact('products', 'brandList', 'subCategory'));
    }

    public function logout_user()
    {
        Auth::logout();
        session()->flush();
        return redirect('/');
    }

    public function profile()
    {
        return view('frontend.user.profile');
    }

    public function profileupdate(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'mobile' => 'required',
        ]);

            $user = User::findorFail(Auth::user()->id);
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->mobile = $request->mobile;
            $user->state = $request->state;
            $user->city = $request->city;
            $user->address = $request->address;

            if($request->hasfile('image'))
            {
                $image = $request->file('image');
                $dest = $user->image;
                if(File::exists($dest))
                {
                    File::delete($dest);
                }
                $filename=time().Str::slug('').'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('upload/profile/');
                $request->image->move($destinationPath, $filename);
                $user->image='upload/profile/'.$filename;
            }
            $user->update();
            return redirect('user/profile');
    }

    public function result(Request $request)
    {
        $searchingData = $request->searchProduct;
        $products = Product::where('name','LIKE','%'.$searchingData.'%')->where('status', ItemStatusTypeEnum::ACTIVE)->first();

        if($products) {
            if(isset($_POST['searchBtn'])) {
                return redirect('collection/'. $products->subSategory->name);
            } else {
                return redirect('collection/'. $products->subCategory->name.'/'.$products->name);
            }
        } else {
            return redirect('/')->with('status','Product Not Available');
        }
    }
}
