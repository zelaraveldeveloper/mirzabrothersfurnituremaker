<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Product;

class CollectionController extends Controller
{
    public function SearchautoComplete(Request $request)
    {
        $query = $request->get('term','');
        $products = Product::where('name','LIKE','%'.$query.'%')->where('status','0')->get();

        $data = [];
        foreach ($products as $items) {
            $data[] = [
                'value'=>$items->name,
                'id'=>$items->id,
            ];
        }
        if(count($data))
        {
            return $data;
        }
        else
        {
            return ['value'=>'No Result Found','id'=>''];
        }
    }
}
