<?php

/**
 * @author Zeeshan N
 */

namespace App\Http\Controllers\Category;

use Exception;
use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Category\CategoryRequest;
use App\Repositories\Interfaces\CategoryInterface;
use App\Http\Requests\Category\CategoryUpdateRequest;

class CategoryController extends Controller
{
    private $categoryRepo;

    public function __construct(CategoryInterface $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * Description - Categories listing
     * @author Zeeshan N
     */
    public function index()
    {
        try {
            $categories = Category::where('userId', Auth::id())->get();
            return view('backend.category.index', compact('categories'));
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description -  Category Store
     * @author Zeeshan N
     */
    public function store(CategoryRequest $request)
    {
        try {
            $this->categoryRepo->store($request->all());
            return response(['success' => true, 'message' => 'Category Saved Successfully.']);
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Category edit
     * @author Zeeshan N
     */
    public function edit($id)
    {
        try {
            $category = $this->categoryRepo->editCategory($id);
            return response()->json(['category' => $category]);
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Category update
     * @author Zeeshan N
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        try {
            $this->categoryRepo->update($request->all(), $id);
            return response(['success' => true, 'message' => 'Category Updated Successfully.']);
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Category delete
     * @author Zeeshan N
     */
    public function destroy($id)
    {
        try {
            $this->categoryRepo->destroy($id);
            return redirect()->route('categories')->with('success', 'Data Deleted Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }
}
