<?php

namespace App\Http\Controllers\User;

use Exception;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Repositories\Interfaces\UserInterface;

class UserController extends Controller
{
    private $userRepo;

    public function __construct(UserInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Description - User listing
     * @author Zeeshan N
     */
    public function index()
    {
        try {
            $users = User::get();
            return view('backend.user.index', compact('users'));
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - User create blade
     * @author Zeeshan N
     */
    public function create()
    {
        try {
            return view('backend.user.create');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - User store
     * @author Zeeshan N
     */
    public function store(UserRequest $request)
    {
        try {
            $user = $this->userRepo->store($request->all());

            if ($user) {
                return redirect()->route('users')->with('success', 'Data Saved Successfully');
            }

            return redirect()->back()->with('error', 'Error While Saving');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - User edit
     * @author Zeeshan N
     */
    public function edit($id)
    {
        try {
            $user = User::findorFail($id);

            if ($user) {
                return view('backend.user.edit', compact('user'));
            }
            return redirect()->back()->with('error', 'Data Not Found');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - User show
     * @author Zeeshan N
     */
    public function show($id)
    {
        try {
            $user = User::findorFail($id);

            if ($user) {
                return view('backend.user.show', compact('user'));
            }
            return redirect()->back()->with('error', 'Data Not Found');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - User update
     * @author Zeeshan N
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $user = $this->userRepo->update($request->all(), $id);

            if ($user) {
                return redirect()->route('users')->with('success', 'Data Updated Successfully');
            }
            return redirect()->back()->with('error', 'Error While Updating');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - User delete
     * @author Zeeshan N
     */
    public function destroy($id)
    {
        try {
            $user = $this->userRepo->destroy($id);
            if ($user) {
                return redirect()->route('users')->with('success', 'Data Deleted Successfully');
            }
            return redirect()->back()->with('error', 'Error While Deleting');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }
}
