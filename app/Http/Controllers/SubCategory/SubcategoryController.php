<?php

namespace App\Http\Controllers\SubCategory;

use Exception;
use App\Models\SubCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SubCategory\SubCategoryRequest;
use App\Repositories\Interfaces\SubCategoryInterface;
use App\Http\Requests\SubCategory\SubCategoryUpdateRequest;

class SubCategoryController extends Controller
{
    private $subCategoryRepo;

    public function __construct(SubCategoryInterface $subCategoryRepo)
    {
        $this->subCategoryRepo = $subCategoryRepo;
    }

    /**
     * Description - SubCategories listing
     * @author Zeeshan N
     */
    public function index()
    {
        try {
            $subCategories = SubCategory::where('userId', Auth::id())->get();
            return view('backend.subCategory.index', compact('subCategories'));
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - SubCategory create blade
     * @author Zeeshan N
     */
    public function create()
    {
        try {
            return view('backend.subCategory.create');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - SubCategory store
     * @author Zeeshan N
     */
    public function store(SubCategoryRequest $request)
    {
        try {
            $this->subCategoryRepo->store($request->all());

            return redirect()->route('subCategories')->with('success', 'Data Saved Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - SubCategory edit
     * @author Zeeshan N
     */
    public function edit($id)
    {
        try {
            $subCategory = SubCategory::findorFail($id);

            return view('backend.subCategory.edit', compact('subCategory'));
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - SubCategory update
     * @author Zeeshan N
     */
    public function update(SubCategoryUpdateRequest $request, $id)
    {
        try {
            $this->subCategoryRepo->update($request->all(), $id);

            return redirect()->route('subCategories')->with('success', 'Data Updated Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - SubCategory delete
     * @author Zeeshan N
     */
    public function destroy($id)
    {
        try {
            $this->subCategoryRepo->destroy($id);
            return redirect()->route('subCategories')->with('success', 'Data Deleted Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }
}
