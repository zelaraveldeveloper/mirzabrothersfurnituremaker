<?php

namespace App\Http\Controllers\Option;

use Exception;
use App\Models\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\OptionInterface;

class OptionController extends Controller
{
    private $optionRepo;

    public function __construct(OptionInterface $optionRepo)
    {
        $this->optionRepo = $optionRepo;
    }

    /**
     * Description - Option page
     * @author Zeeshan N
     */
    public function index()
    {
        try {
            $option = Option::first();
            return view('backend.option.index', compact('option'));
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Option update
     * @author Zeeshan N
     */
    public function update(Request $request, $id)
    {
        try {
            $this->optionRepo->update($request->all(), $id);
            return redirect()->route('options')->with('success', 'Data Updated Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }
}
