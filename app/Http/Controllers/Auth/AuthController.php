<?php

namespace App\Http\Controllers\Auth;

use Exception;
use App\Models\User;
use App\Models\State;
use App\Enums\UserRoleEnum;
use Illuminate\Support\Str;
use App\Enums\UserStatusEnum;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\SignInRequest;
use App\Http\Requests\Auth\SignUpRequest;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use App\Repositories\Repository\General\GeneralRepository;

class AuthController extends Controller
{
    public $generalRepo;

    /**
     * Constructor function.
     */
    public function __construct()
    {
        $this->generalRepo = new GeneralRepository();
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(SignInRequest $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $remember = $request->input('remember') ? true : false;

            if (Auth::attempt($credentials, $remember)) {
                if (Auth::user()->role == UserRoleEnum::ADMIN) {
                    return redirect()->route('dashboard');

                } elseif (Auth::user()->role == UserRoleEnum::USER) {
                    return redirect()->back()->with('status','You are Logged in successfully');
                }
                return back()->withErrors(['error' => 'Not Authorized!']);
            }

            return back()->withErrors(['email' => 'Credentials mismatch!'])->onlyInput('email');
        } catch (Exception $e) {
            return redirect()->route('logout');
        }
    }

    public function logout()
    {
        Auth::logout();
        session()->flush();
        return redirect()->route('home');
    }

    public function states($id)
    {
        try {
            $states = State::where('countryId', $id)->get();

            return response(['success' => true, 'states' => $states]);
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    public function signUp(SignUpRequest $request)
    {
        try {
            DB::beginTransaction();
            if (isset($request->image)) {
                $fileName = Str::random(10) . '.' . $request->image->getClientOriginalExtension();
                $destinationPath = public_path('upload/user/images');
                $request->image->move($destinationPath, $fileName);
            }


            $user = new User();
            $user->fullName = $request->fullName;
            $user->mobile = $request->mobile;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->countryId = $request->countryId;
            $user->stateId = $request->stateId;
            $user->city = $request->city;
            $user->address = $request->address;
            $user->image = $fileName;
            $user->status = UserStatusEnum::APPROVED;
            $user->role = UserRoleEnum::USER;
            $user->save();

            DB::commit();
            Auth::login($user);
            return redirect()->route('userHome');
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }

    }

    public function changePassword()
    {
        return view('backend.changePassword.index');
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = User::find(Auth::id());
        $user->password = Hash::make($request->newPassword);
        $user->update();
        return redirect()->route('changePassword')->with('success', 'Password Update Successfully');
    }
}
