<?php

namespace App\Http\Controllers\Admin;
use App\Models\User;
use App\Models\Group;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function vendor()
    {
        return view('backend.vendor.login');
    }

    public function profileupdate(Request $request)
    {
        $user = User::findorFail(Auth::user()->id);
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->mobile = $request->mobile;
        $user->state = $request->state;
        $user->city = $request->city;
        $user->address = $request->address;

        if($request->hasfile('image'))
        {
            $image = $request->file('image');
            $dest = $user->image;
            if(File::exists($dest))
            {
                File::delete($dest);
            }
            $filename=time().Str::slug('').'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('upload/profile/');
            $request->image->move($destinationPath, $filename);
            $user->image='upload/profile/'.$filename;
        }
        $user->update();
        return redirect('admin/dashboard');
    }

    public function change_password()
    {
        return view('backend.change_password');
    }
}
