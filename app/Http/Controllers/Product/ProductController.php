<?php

/**
 * @author Zeeshan N
 */

namespace App\Http\Controllers\Product;

use Exception;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Product\ProductRequest;
use App\Repositories\Interfaces\ProductInterface;
use App\Http\Requests\Product\ProductUpdateRequest;

class ProductController extends Controller
{
    private $productRepo;

    public function __construct(ProductInterface $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    /**
     * Description - Products listing
     * @author Zeeshan N
     */
    public function index()
    {
        try {
            $products = Product::where('userId', Auth::id())->get();
            return view('backend.product.index', compact('products'));
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Product create blade
     * @author Zeeshan N
     */
    public function create()
    {
        try {
            return view('backend.product.create');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Product store
     * @author Zeeshan N
     */
    public function store(ProductRequest $request)
    {
        try {
            $isNew = $request->input('isNew') == true ? '1' : '0';
            $isPopular = $request->input('isPopular') == true ? '1' : '0';
            $this->productRepo->store($request->all(), $isNew, $isPopular);

            return redirect()->route('products')->with('success', 'Data Saved Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Product edit
     * @author Zeeshan N
     */
    public function edit($id)
    {
        try {
            $product = Product::findorFail($id);

            if ($product) {
                return view('backend.product.edit', compact('product'));
            }
            return redirect()->back()->with('error', 'Data Not Found');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Product update
     * @author Zeeshan N
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        try {
            $isNew = $request->input('isNew') == true ? '1' : '0';
            $isPopular = $request->input('isPopular') == true ? '1' : '0';

            $this->productRepo->update($request->all(), $isNew, $isPopular, $id);

            return redirect()->route('products')->with('success', 'Data Updated Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Product delete
     * @author Zeeshan N
     */
    public function destroy($id)
    {
        try {
            $this->productRepo->destroy($id);
            return redirect()->route('products')->with('success', 'Data Deleted Successfully');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }
}
