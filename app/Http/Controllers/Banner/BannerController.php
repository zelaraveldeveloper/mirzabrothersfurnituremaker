<?php

namespace App\Http\Controllers\Banner;

use Exception;
use App\Models\Banner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\BannerRequest;
use App\Repositories\Interfaces\BannerInterface;
use App\Http\Requests\Banner\BannerUpdateRequest;

class BannerController extends Controller
{
    private $bannerRepo;

    public function __construct(BannerInterface $bannerRepo)
    {
        $this->bannerRepo = $bannerRepo;
    }

    /**
     * Description - Banner listing
     * @author Zeeshan N
     */
    public function index()
    {
        try {
            $banners = Banner::get();
            return view('backend.banner.index', compact('banners'));

        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Banner create blade
     * @author Zeeshan N
     */
    public function create()
    {
        try {
            return view('backend.banner.create');

        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Banner store
     * @author Zeeshan N
     */
    public function store(BannerRequest $request)
    {
        try {
            $user = $this->bannerRepo->store($request->all());

            if ($user) {
                return redirect()->route('banners')->with('success', 'Data Saved Successfully');
            }

            return redirect()->back()->with('error', 'Error While Saving');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Banner edit
     * @author Zeeshan N
     */
    public function edit($id)
    {
        try {
            $banner = Banner::findorFail($id);

            if ($banner) {
                return view('backend.banner.edit',compact('banner'));
            }
            return redirect()->back()->with('error', 'Data Not Found');
        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Banner update
     * @author Zeeshan N
     */
    public function update(BannerUpdateRequest $request, $id)
    {
        try {
            $user = $this->bannerRepo->update($request->all(), $id);

            if ($user) {
                return redirect()->route('banners')->with('success', 'Data Updated Successfully');
            }

            return redirect()->back()->with('error', 'Error While Updating');

        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }

    /**
     * Description - Banner delete
     * @author Zeeshan N
     */
    public function destroy($id)
    {
        try {
            $user = $this->bannerRepo->destroy($id);

            if ($user) {
                return redirect()->route('banners')->with('success', 'Data Deleted Successfully');
            }
            return redirect()->back()->with('error', 'Error While Deleting');

        } catch (Exception $e) {
            return response()->json(['response' => ['status' => false, 'message' => $e->getMessage()]]);
        }
    }
}
