<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'countryId',
        'fullName',
        'stateId',
        'address',
        'mobile',
        'email',
        'city',
    ];

    protected $table = "users";

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the country associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country(): HasOne
    {
        return $this->hasOne(Country::class, 'id', 'countryId');
    }

    public function isUserOnline()
    {
        return Cache::has('user-is-online' . $this->id);
    }

    /**
     * Get the state associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function state(): HasOne
    {
        return $this->hasOne(State::class, 'id', 'stateId');
    }
}
