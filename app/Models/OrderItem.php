<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $filable = [
        'productId',
        'orderId',
        'quantiy',
        'taxAmt',
        'price',
    ];

    protected $table = "orderitems";

    public function product()
    {
        return $this->belongsTo(Product::class,'productId','id');
    }

}
