<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'smallDescription',
        'subCategoryId',
        'originalPrice',
        'categoryId',
        'offerPrice',
        'salePrice',
        'brandId',
        'isPopular',
        'quantity',
        'status',
        'userId',
        'isNew',
        'name',
    ];

    protected $table = "products";

    protected function getImageAttribute($value)
    {
        return 'upload/product/images/' . $value;
    }

    /**
     * Get the category that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'categoryId', 'id');
    }

    /**
     * Get the subCategory that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subCategory(): BelongsTo
    {
        return $this->belongsTo(SubCategory::class, 'subCategoryId','id');
    }

    /**
     * Get the brand that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brandId', 'id');
    }

    // public function group()
    // {
    //     return $this->belongsTo(Group::class);
    // }

}
