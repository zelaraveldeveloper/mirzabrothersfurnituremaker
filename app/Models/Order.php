<?php

namespace App\Models;

use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $filable = [
        'userId',
        'trackingNo',
        'trackingMsg',
        'paymentId',
        'paymentStatus',
        'paymentMode',
        'orderStatus',
        'cancelReason',
    ];

    protected $table = "orders";

    public function users()
    {
        return $this->belongsTo(User::class, 'userId', 'id');
    }

    public function orderitems()
    {
        return $this->hasMany(OrderItem::class, 'orderId', 'id');
    }
}
