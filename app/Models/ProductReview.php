<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductReview extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable=[
        'productId',
        'review',
        'userId',
        'status',
        'rate',
    ];

    protected $table = "productreviews";
}
