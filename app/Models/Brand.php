<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Brand extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'categoryId',
        'userId',
        'name',
    ];

    protected $table = "brands";

    /**
     * Get the category that owns the Brand
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'categoryId', 'id');
    }

    // public function category()
    // {
    //     return $this->hasMany(Category::class);
    // }

    // public function subcategory()
    // {
    //     return $this->hasMany(Subcategory::class);
    // }

    // public function product()
    // {
    //     return $this->hasMany(Product::class);
    // }
}
