<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Option extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'description',
        'footerText',
        'instagram',
        'facebook',
        'currency',
        'address',
        'email',
        'title',
        'phone',
	];

    protected $table = "options";
}
