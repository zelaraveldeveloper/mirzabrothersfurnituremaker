<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Wishlist extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillables = [
        'userId',
        'productId',
        'status',
    ];

    protected $table = 'wishlists';

    public function product()
    {
        return $this->belongsTo(Product::class, 'productId', 'id');
    }
}
