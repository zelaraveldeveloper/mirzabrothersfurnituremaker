<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Banner extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'description',
        'status',
        'title',
    ];

    protected $table = "banners";

    protected function getImageAttribute($value)
    {
        return 'upload/banner/images/' . $value;
    }
}
