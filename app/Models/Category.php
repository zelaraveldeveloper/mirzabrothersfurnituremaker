<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'userId',
        'name',
    ];

    protected $table = "categories";

    // public function subcategory()
    // {
    //     return $this->hasMany(Subcategory::class);
    // }

    // public function product()
    // {
    //     return $this->hasMany(Product::class);
    // }
}
