<?php

namespace App\Providers;

use App\Models\Option;
use App\Models\SubCategory;
use App\Enums\ItemStatusTypeEnum;
use App\Models\Country;
use Illuminate\Support\ServiceProvider;

class OptionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('globalData', function ($app) {
            $option = Option::first();
            $subCategories = SubCategory::take('5')->get();
            $countries = Country::get();
            return ['option' => $option, 'subCategories' => $subCategories, 'countries' => $countries];
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
