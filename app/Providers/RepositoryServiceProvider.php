<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\BrandInterface;
use App\Repositories\Repository\UserRepository;
use App\Repositories\Interfaces\BannerInterface;
use App\Repositories\Interfaces\OptionInterface;
use App\Repositories\Repository\BrandRepository;
use App\Repositories\Interfaces\ProductInterface;
use App\Repositories\Repository\BannerRepository;
use App\Repositories\Repository\OptionRepository;
use App\Repositories\Interfaces\CategoryInterface;
use App\Repositories\Repository\ProductRepository;
use App\Repositories\Repository\CategoryRepository;
use App\Repositories\Interfaces\SubCategoryInterface;
use App\Repositories\Repository\SubCategoryRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BrandInterface::class, BrandRepository::class);
        $this->app->bind(CategoryInterface::class, CategoryRepository::class);
        $this->app->bind(SubCategoryInterface::class, SubCategoryRepository::class);
        $this->app->bind(ProductInterface::class, ProductRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(OptionInterface::class, OptionRepository::class);
        $this->app->bind(BannerInterface::class, BannerRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
